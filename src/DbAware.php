<?php
/**
 * @copyright	2014 - 2024 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza;

/**
 * Provide static methods for manage a DbConnection object.
 * This trait provide the functionality defined by DbConnectable.
 *
 * @package xibalba\tuza
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
trait DbAware {
	/**
	 * @var DbConnectionInterface|null Object.
	 */
	protected static ?DbConnectionInterface $_db = null;

	/**
	 * Set a DbConnection instance
	 *
	 * @param DbConnectionInterface $db Object
	 */
	public static function setDbConnection(DbConnectionInterface $db) : void {
		static::$_db = $db;
	}

	/**
	 * @return DbConnectionInterface instance.
	 */
	public static function getDbConnection() : DbConnectionInterface {
		if(static::$_db !== null) return static::$_db;
		else throw new \Exception('«$_db» is null, must be a xibalba\tuza\DbConnectionInterface instance.');
	}
}
