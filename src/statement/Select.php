<?php
/**
 * @copyright	2014 - 2020 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\statement;

use xibalba\tuza\clause\dialects\Top;
use xibalba\tuza\clause\Join;
use xibalba\tuza\clause\traits\WhereAware;
use xibalba\tuza\clause\OrderBy as OrderByClause;
use xibalba\tuza\clause\Join as JoinClause;
use xibalba\tuza\clause\dialects\Limit as LimitClause;
use xibalba\tuza\clause\dialects\Top as TopClause;

use xibalba\tuza\expression\Columns as ColumnsExpression;
use xibalba\tuza\expression\traits\ColumnsAware as ColumnsExpressionAware;

use xibalba\tuza\syntax\Target as TargetSyntax;
use xibalba\tuza\syntax\Column as ColumnSyntax;

use xibalba\tuza\syntax\Factory as SyntaxFactory;

use \Ds\Vector;

class Select {
	use ColumnsExpressionAware, WhereAware;

	protected $_orderByClauses;
	protected $_joinClauses;

	protected $_isDistinct;

	// Dialect clauses
	protected $_limitClause;
	protected $_topClause;

	public function __construct(?Vector $columns = null, ?TargetSyntax $target = null) {
		$this->_isDistinct = false;

		if(!empty($columns)) {
			$this->_columnsExpression = new ColumnsExpression($columns);
			$this->setTarget($target);
		}
	}

	// DISTINCT support API

	public function distinct() {
		$this->_isDistinct = true;
	}

	public function isDistinct() : bool {
		return $this->_isDistinct;
	}

	// COLUMNS expression API

	public function setColumns(Vector $columns) {
		$this->getColumnsExpression()->setColumns($columns);
	}

	public function getColumns() : Vector {
		return $this->getColumnsExpression()->getColumns();
	}

	public function getColumn(string $name) : ColumnSyntax {
		$columns = $this->getColumns();

		// Check for a single ALL column
		if($columns->count() === 1 && $columns->get(0)->isAll()) {
			// Then create a "fake" ColumnSyntax with provided name
			return SyntaxFactory::createColumn($name, null, $this->getTarget());
		}

		// Otherwise, check for specific name
		foreach ($columns as $c) {
			if($c->getName() == $name) return $c;
		}

		throw new \InvalidArgumentException("Provided column name '$name' not found on Select Statement instance.");
	}

	public function hasColumnsExpression() : bool {
		return ($this->_columnsExpression instanceof ColumnsExpression);
	}

	public function getAllColumns() : Vector {
		$cols = $this->getColumns();

		if($this->hasJoin()) {
			$allJoins = $this->getAllJoins();
			foreach($allJoins as $join) {
				if($join->hasColumnsExpression()) $cols->push(...$join->getColumnsExpression()->getColumns());
			}
		}

		return $cols;
	}

	// JOIN cluases API
	protected  function _addJoin(string $target, string $joinType, $selecteableColumns = null) : JoinClause {
		$join = new JoinClause(SyntaxFactory::createTarget($target), $joinType);

		if(!is_null($selecteableColumns)) $join->setColumns($selecteableColumns);

		if(is_null($this->_joinClauses)) $this->_joinClauses = new Vector();
		$this->_joinClauses->push($join);

		return $join;
	}

	public function innerJoin(string $target, $selecteableColumns = null) : JoinClause {
		return $this->_addJoin($target, JoinClause::JOIN_INNER, $selecteableColumns);
	}

	public function leftJoin(string $target, $selecteableColumns = null) : JoinClause {
		return $this->_addJoin($target, JoinClause::JOIN_LEFT, $selecteableColumns);
	}

	public function rightJoin(string $target, $selecteableColumns = null) : JoinClause {
		return $this->_addJoin($target, JoinClause::JOIN_RIGHT, $selecteableColumns);
	}

	public function crossJoin(string $target, $selecteableColumns = null) : JoinClause {
		return $this->_addJoin($target, JoinClause::JOIN_CROSS, $selecteableColumns);
	}

	public function hasJoin() : bool {
		return $this->_joinClauses instanceof Vector && $this->_joinClauses->count() > 0;
	}

	public function getAllJoins() : Vector {
		return $this->_joinClauses;
	}

	// ORDER BY clause API

	public function orderBy(Vector $columns, string $direction) {
		if(is_null($this->_orderByClauses)) $this->_orderByClauses = new Vector();
		$this->_orderByClauses->push(new OrderByClause($columns, $direction));
	}

	public function getOrderBy() : Vector {
		return $this->_orderByClauses;
	}

	public function hasOrderBy() {
		return $this->_orderByClauses instanceof Vector && $this->_orderByClauses->count() > 0;
	}

	// LIMIT clause API

	public function setLimit(int $limit, ?int $offset = null) {
		if(!$this->hasLimit()) $this->_limitClause = new LimitClause($limit, $offset);
		else {
			$this->_limitClause->setLimit($limit);
			$this->_limitClause->setOffset($offset);
		}
	}

	public function hasLimit() : bool {
		return $this->_limitClause instanceof LimitClause;
	}

	public function getLimit() : LimitClause {
		return $this->_limitClause;
	}

	// TOP clause API

	public function setTop(int $top) {
		if(!$this->hasTop()) $this->_topClause = new TopClause($top);
		else $this->_topClause->setTop($top);
	}

	public function hasTop() : bool {
		return $this->_topClause instanceof TopClause;
	}

	public function getTop() : TopClause {
		return $this->_topClause;
	}
}
