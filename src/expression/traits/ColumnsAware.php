<?php
/**
 * @copyright	2014 - 2023 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\expression\traits;

use xibalba\tuza\syntax\traits\TargetAware;
use xibalba\tuza\expression\Columns as ColumnsExpression;

trait ColumnsAware {
	use TargetAware;

	protected ?ColumnsExpression $_columnsExpression = null;

	/**
	 * @param $colmns ColumnsExpression
	 */
	public function setColumnsExpression(ColumnsExpression $columns) {
		$this->_columnsExpression = $columns;
	}

	/**
	 * @return ColumnsExpression
	 */
	public function getColumnsExpression() : ColumnsExpression {
		if (!$this->hasTarget()) {
			throw new \RuntimeException("A target (table or view) must be setted before define the column expression.");
		}

		if (is_null($this->_columnsExpression)) {
			throw new \RuntimeException("You must set at least a '*' or a list of columns for selection.");
		}

		return $this->_columnsExpression;
	}

	/**
	 * @return bool
	 */
	public function hasColumnsExpression() : bool {
		return $this->_columnsExpression instanceof ColumnsExpression;
	}
}