<?php
/**
 * @copyright	2014 - 2024 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza;

use \RuntimeException as PhpException;

/**
 * Exception thrown if some error happend during an query execution.
 *
 * @package xibalba\tuza
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class DbException extends PhpException {

	protected string $_query = '';

	/**
	 * {@inheritdoc}
	 */
	public function __construct(string $message = "", string $query = "", PhpException $previus = null) {
		$message .= (isset($previus)) ? " : " . $previus->getMessage() : "";
		$code = (isset($previus)) ? $previus->getCode() : 0;

		// Call parent with code zero sice a int type is required
		parent::__construct($message, 0, $previus);

		// Set code by hand since code can be a string
		$this->code = $code;
		$this->_query = $query;
	}

	public function getQueryStatement() : string {
		return $this->_query;
	}
}
