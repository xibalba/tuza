<?php

namespace guatuza;

use PHPUnit\Framework\TestCase;

use xibalba\tuza\DbConnection;

use xibalba\tuza\builder\Insert as InsertBuilder;
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\builder\Delete as DeleteBuilder;

use \PDOStatement;
use \Ds\Map;

class DbConnectionTest extends TestCase {

	private $__conn;

	public function setUp() : void {
		$this->__conn = new DbConnection(new Map([
			'database_type' => 'sqlite',
			'database_file' => TEST_DIR . '/guatuza.db'
		]));
	}

	public function testExec() {
		$qd = "DROP TABLE IF EXISTS tbl_foo";
		$qc = "CREATE TABLE tbl_foo (col_a TEXT, col_b TEXT, field_c TEXT)";

		// Create table for test
		$stmD = $this->__conn->exec($qd);
		$stmC = $this->__conn->exec($qc);

		$this->assertSame($qd, $stmD->queryString);
		$this->assertSame($qc, $stmC->queryString);
	}

	/**
	 * @depends testExec
	 */
	public function testInsert() {
		$qb = new InsertBuilder("tbl_foo");

		$datas = [
			["col_a" => "value_a_1", "col_b" => "value_b_1", "field_c" => "value_c_1"],
			["col_a" => "value_a_2", "col_b" => "value_b_2", "field_c" => "value_c_2"],
			["col_a" => "value_a_3", "col_b" => "value_b_3", "field_c" => "value_c_3"]
		];

		$qb->pushValues($datas);

		$res = $this->__conn->exec($qb->getSql());

		$this->assertSame(3, $res->rowCount());

		return $datas;
	}

	/* *
	 * @depends testInsert
	 */
	/*public function testSelectExec($datas) {
		$sb = new SelectBuilder("tbl_foo");
		$sb->setColumns("col_a", "col_b", "field_c");

		$fetched = $this->__conn->exec($sb->getSql());
		
		$expected = $datas[0];
		$r0 = $fetched->fetch(\PDO::FETCH_ASSOC);

		$this->assertSame($expected, $r0);
	}*/

	/* *
	 * @depends testSelectExec
	 */
	/*public function testDelete() {
		echo "delete";
		$db = new DeleteBuilder("tbl_foo");
		var_dump($db->getSql());
		$res = $this->__conn->exec($db->getSql());
		$this->assertSame(3, $res);
	}*/
}
