<?php
/**
 * @copyright	2014 - 2023 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\expression;

use xibalba\tuza\syntax\Column as ColumnSyntax;

use \Ds\Vector;
use \Ds\Map;

/**
 * A Columns expression is a set of "column syntax" elemens separated by commas,
 * so the class each column syntax element are stores into a Vector.
 * SELECT is not the only structure that support a column expression,
 * also does it the JOIN and other statements.
 */
class Columns {

	protected Vector $_columns;
	protected Vector $_functionAsColumns;

	function __construct(Vector $columns = new Vector()) {
		$this->setColumns($columns);
		$this->_functionAsColumns = new Vector();
	}

	/**
	 * Add to query a function to be added on columns part as a `SELECT` statement,
	 * example: `SELECT [...] functionName(arguments) AS alias, ...`
	 *
	 * @param string $functionName
	 * @param $arguments SQL function arguments
	 * @param $alias Alias string
	 */
	public function addFunction(string $functionName, array|Vector $arguments = new Vector(), string $alias = "") {
		if(
			is_array($arguments) 
			&& array_keys($arguments) === range(0, count($arguments) - 1)
		) $arguments = new Vector($arguments);
		else throw new \InvalidArgumentException("Function's arguments must be expressed as a indexed array of strings or a Vector instance.");

		$this->getColumnsFunction()
			->push(new Map(["function" => $functionName, "args" => $arguments, "alias" => $alias]));
	}

	/**
	 * Set the columns vector. Ensure that Vector values ar instances of syntax\Colmun.
	 * 
	 * @param $columns Vector
	 * 
	 * @return Columns
	 */
	public function setColumns(Vector $columns) : Columns {
		$this->_columns = $columns->filter(
			function($value) { 
				return $value instanceof ColumnSyntax; 
			}
		);

		return $this;
	}

	/**
	 * Set the alias for a specific column by name.
	 * 
	 * @param $columns sring Column name.
	 * @param $alias string Alias for the column.
	 * 
	 * @return Columns
	 */
	public function setColumnAlias(string $column, string $alias) : Columns {
		foreach ($this->_columns as $coll) {
			if ($coll->getName() === $column) {
				$coll->setAlias($alias);
				return $this;
			}
		}

		return $this;
	}

	/**
	 * @return Vector
	 */
	public function getColumnsFunction() : Vector {
		return $this->_functionAsColumns;
	}

	/**
	 * @return Vector
	 */
	public function getColumns() : Vector {
		return $this->_columns;
	}
}
