<?php

namespace guatuza\composer;

use PHPUnit\Framework\TestCase;

use xibalba\tuza\clause\Where as WhereClause;
use xibalba\tuza\syntax\Factory as SyntaxFactory;
use xibalba\tuza\syntax\Target as TargetSyntax;

use xibalba\tuza\composer\Where as WhereComposer;

use Ds\Map;
use Ds\Vector;

class WhereTest extends TestCase {

	public function testSingleComparison() {
		$expected = "(generic.foo = 'fake')";

		$target = new TargetSyntax("generic");
		$where = new WhereClause($target);

		$where->equals("foo", "fake");

		$this->assertSame($expected, WhereComposer::composeComparisons($where));
	}

	public function testBetween() {
		$expected = "(generic.foo BETWEEN 66 AND generic.baz) AND (66 NOT BETWEEN generic.foo AND '20240606')";

		$target = new TargetSyntax("generic");
		$where = new WhereClause($target);

		$where->between(
				SyntaxFactory::createColumn("foo", null, $target),
				66,
				SyntaxFactory::createColumn( "baz", null, $target),
			)
			->notBetween(
				66,
				SyntaxFactory::createColumn("foo", null, $target),
				'20240606'
			);

		$this->assertSame($expected, WhereComposer::composeComparisons($where));
	}

	public function testColumnBetweenValues() {
		$expected = "(generic.foo BETWEEN 66 AND 99)";

		$target = new TargetSyntax("generic");
		$where = new WhereClause($target);

		$where->columnBetweenValues("foo", 66, 99);
		$this->assertSame($expected, WhereComposer::composeComparisons($where));
	}

	public function testValueBetweenColumns() {
		$expected = "(66 BETWEEN generic.foo AND generic.baz)";

		$target = new TargetSyntax("generic");
		$where = new WhereClause($target);

		$where->valueBetweenColumns(66, "foo", "baz");
		$this->assertSame($expected, WhereComposer::composeComparisons($where));
	}

	public function testConjunction() {
		$expected = "(generic.foo = 'fake') OR (generic.bar > 100) OR (generic.baz <= 10)";

		$target = new TargetSyntax("generic");
		$where = new WhereClause($target, WhereClause::CONJUNCTION_OR);

		$where->equals("foo", "fake")
			->greaterThan("bar", 100)
			->lessThanOrEqual("baz", 10);

		$this->assertSame($expected, WhereComposer::composeComparisons($where));
	}

	public function testSubWhere() {
		$expected = "(generic.foo = 'fake') AND ( (generic.bar > 100) OR (generic.baz <= 10) )";

		$target = new TargetSyntax("generic");
		$where = new WhereClause($target);

		$where->equals("foo", "fake")
			->subWhere(WhereClause::CONJUNCTION_OR)
			->greaterThan("bar", 100)
			->lessThanOrEqual("baz", 10);

		$this->assertSame($expected, WhereComposer::compose($where));
	}

	public function testIns() {
		$expected = "(generic.foo IN ('a', 'b', 'c')) AND (generic.baz NOT IN ('x', 100, 0))";

		$target = new TargetSyntax("generic");
		$where = new WhereClause($target);

		$where->in("foo", new Vector(["a", "b", "c"]))
			->notIn("baz", new Vector(["x", 100, false]));

		$this->assertSame($expected, WhereComposer::composeComparisons($where));
	}

	public function testIsNullAndNotNull() {
		$expected = "(generic.foo IS NULL) AND (generic.bar IS NOT NULL)";
		$target = new TargetSyntax("generic");
		$where = new WhereClause($target);

		$where->isNull("foo")
			->isNotNull("bar");

		$this->assertSame($expected, WhereComposer::composeComparisons($where));
	}

}