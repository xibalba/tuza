<?php
/**
 * @copyright	2014 - 2021 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\statement;

use xibalba\tuza\syntax\traits\TargetAware;
use xibalba\tuza\clause\traits\WhereAware;

class Delete {
	use TargetAware, WhereAware;
}