<?php
/**
 * @copyright	2014 - 2020 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\statement\interfaces;

use xibalba\tuza\syntax\Target as TableSyntax;

/**
 * This interface define the basic form that any statement must implement.
 */
interface Query {
	public function getPartName() : string;

	public function getTable(TableSyntax $table);

	// public function getWhere() : WhereClause;
}
