<?php
/**
 * @copyright	2014 - 2024 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza;

use \PDO;
use \PDOStatement;

use \Ds\Vector;
use \Ds\Map;

/**
 * Class DbConnection.
 * This class provide an Object-Oriented interface to facilitate the
 * interaction with database engines.
 * It is just a light wrapper over PDF in order to facilitate the
 * SQL execution.
 *
 * @package xibalba\tuza
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class DbConnection implements DbConnectionInterface {
	/**
	 * @var string Database Type
	 */
	protected string $_databaseType;

	/**
	 * @var PDO PDO Instance.
	 */
	protected PDO $_pdo;

	/**
	 * @var array Options to be assigned to PDO at creation time.
	 */
	protected array $_pdoOption;

	/**
	 * @var Vector Vector of statement performed.
	 */
	protected Vector $_logs;

	/**
	 * @var bool Debug mode flag.
	 */
	protected bool $_debugMode = false;

	/**
	 * @var PDOStatement instance
	 */
	protected PDOStatement $_statement;

	/**
	 * DbConnection constructor.
	 * @param Map $options
	 */
	public function __construct(Map $options) {
		$this->_logs = new Vector();

		$this->setOptions($options);

		try {
			switch ($this->_databaseType) {
				// For SQLite build immediately the PDO object and return this DbConnection instance.
				case 'sqlite':
					$this->_configureSQLite($options);
					break;

				// MariaDB and MySQL share configuration way
				case 'mariadb':
				case 'mysql':
					$this->_configureMySQL($options);
					break;

				case 'pgsql':
					$this->_configurePg($options);
					break;

				// Microsoft SQl Server is a bit special
				case 'sybase':
				case 'dblib':
				case 'sqlsrv':
					$this->_configureMs($options);
					break;
			}
		}
		catch (\PDOException $e) {
			throw new DbException($e->getMessage(), "", $e);
		}
	}

	/* *****************************************************************************
	 * Protected methods called by constructor
	 * *****************************************************************************/

	protected function _configureSQLite(Map $options) : void {
		if(!$options->hasKey("database_file")) throw new \InvalidArgumentException("For a SQLite connection you must provide a 'database_file' option.");
		$this->_pdo = new PDO('sqlite:' . $options->get("database_file"), null, null, $this->_pdoOption);
	}

	protected function _configureMySQL(Map $options) : void {
		$attr = new Map();
		$commands = new Vector();

		$attr->put("driver", "mysql");
		$attr->put("dbname", $options->get("database_name"));

		if ($options->hasKey("socket")) $attr->put("unix_socket", $options->get("socket"));
		else $attr->put("host", $options->get("server"));

		// Ensure for to use standard quoted identifier
		$commands->push("SET SQL_MODE=ANSI_QUOTES");

		if($options->hasKey("charset")) $commands->push("SET NAMES '" . $options->get("charset") . "'");

		$this->_createPdo($attr, $options->get("username"), $options->get("password"));

		foreach($commands as $value) $this->getPdo()->exec($value);
	}

	protected function _configurePg(Map $options) : void {
		$attr = new Map();

		$attr->put("driver", "pgsql");
		$attr->put("dbname", $options->get("database_name"));
		$attr->put("host", $options->get("server"));

		$this->_createPdo($attr, $options->get("username"), $options->get("password"));
	}

	protected function _configureMs(Map $options) : void {
		$attr = new Map();
		$commands = new Vector();

		$msDriver = ($this->_databaseType === 'sybase') ? 'dblib' : $this->_databaseType;

		$dbKeyStr = ($msDriver === 'sqlsrv') ? 'database' : 'dbname';
		$serverKeyStr = ($msDriver === 'sqlsrv') ? 'server' : 'host';

		$attr->put("driver", $msDriver);
		$attr->put($dbKeyStr, $options->get("database_name"));
		$attr->put($serverKeyStr, $options->get("server"));

		if($options->hasKey("port")) {
			$port = $options->get("port");
			if(!is_int($port)) throw new \InvalidArgumentException("port option must be a integer value");
			$attr->put("port", $port);
		}

		if($msDriver === 'dblib' && $options->hasKey("charset")) $attr->put("charset", $options->get("charset"));

		// Keep MSSQL QUOTED_IDENTIFIER is ON for standard quoting
		$commands[] = 'SET QUOTED_IDENTIFIER ON';

		// Make ANSI_NULLS is ON for NULL value
		$commands[] = 'SET ANSI_NULLS ON';

		$this->_createPdo($attr, $options->get("username"), $options->get("password"));

		// Microsoft `sqlsrv` driver require extra handling
		if($this->_databaseType == 'sqlsrv') {
			if($options->hasKey("charset")) {
				if($options->get("charset") === 'utf8') {
					$this->getPdo()->setAttribute(PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_UTF8);
				}
				else if($options->get("charset") === 'iso-8859-1') {
					$this->getPdo()->setAttribute(PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_SYSTEM);
				}
				else throw new \InvalidArgumentException("Unsuported charset for 'sqlsrv' driver.");
			}
			else {
				// By default ensure UTF8 encoding
				$this->getPdo()->setAttribute(PDO::SQLSRV_ATTR_ENCODING, PDO::SQLSRV_ENCODING_UTF8);
			}
		}

		foreach($commands as $value) $this->getPdo()->exec($value);
	}

	protected function _createPdo(Map $attr, string $username, string $password) : void {
		// Take apart the driver from `$attr`
		$driver = $attr->remove("driver");

		// Prepare a stack for build the `$dsn` string
		$stack = new Vector();
		foreach ($attr as $key => $value) {
			if (is_int($key)) $stack->push($value);
			else $stack->push($key . '=' . $value);
		}

		// This build the `$dsn` like '<driver>:server=<ip|name>databe=<some_name>[...]'
		$dsn = $driver . ':' . $stack->join(";");

		// Create the PDO object
		$this->_pdo = new PDO(
			$dsn,
			$username,
			$password,
			$this->_pdoOption
		);

		// Set as default error mode the exception way
		$this->_pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}

	/* *****************************************************************************
	 * Protected methods
	 * *****************************************************************************/

	/**
	 * Set the properties values in function of option array.
	 *
	 * @param Map $options Options to be set.
	 */
	protected function setOptions(Map $options) : void {
		if(!$options->hasKey("database_type")) throw new \DomainException("Must define a 'database_type' as configuration option.");

		$this->_databaseType = strtolower($options->get("database_type"));
		$this->_pdoOption = $options->get("option", []);
		$this->_debugMode = $options->get("debug_mode", false);
	}

	/* *****************************************************************************
	 *	Public api methods
	 * *****************************************************************************/

	/**
	 * Execute an SQL statement and return the `PDOStatement` created object.
	 * If fail then Rise a `DbException`.
	 *
	 * @param string $query
	 * @return PDOStatement Statement object created for execution.
	 * @throws DbException
	 */
	public function exec(string $query) : PDOStatement {
		$this->_logs->push($query);

		try {
			$this->_statement = $this->getPdo()->prepare($query);
			$this->_statement->execute();
			return $this->_statement;
		}
		catch(\PDOException $e) {
			$msg = "Statement execution fail";
			if($this->_debugMode) throw new DbException($msg . ".", $query, $e);
			else throw new DbException($msg . ": " . $e->getMessage(), $query);
		}
	}

	/**
	 * @inheritdoc
	 */
	public function query(string $query) : PDOStatement {
		return $this->exec($query);
	}

	/**
	 * Quotes a string for use in a query.
	 *
	 * @param string $string String to be qouted.
	 * @return string | bool The qoted string of false if the driver does not support quoting.
	 */
	public function quote(string $string) {
		return $this->_pdo->quote($string);
	}

	public function debug() {
		$this->_debugMode = true;
		return $this;
	}

	/**
	 * @inheritdoc
	 */
	public function getError() : ?array {
		return $this->_statement?->errorInfo();
	}

	public function info() : array {
		$output = [
			'server' => 'SERVER_INFO',
			'driver' => 'DRIVER_NAME',
			'client' => 'CLIENT_VERSION',
			'version' => 'SERVER_VERSION'
		];

		foreach ($output as $key => $value) {
			$output[$key] = $this->_pdo->getAttribute(constant('PDO::ATTR_' . $value));
		}

		return $output;
	}

	/**
	 * Returns the ID of the last inserted row, or the last value from a sequence object,
	 * depending on the underlying driver.
	 */
	public function getLastInsertId() {
		if(in_array($this->_databaseType, ['sybase', 'dblib', 'sqlsrv'], true)) return $this->_pdo->query('SELECT SCOPE_IDENTITY()')->fetchColumn();
		return $this->_pdo->lastInsertId();
	}

	/**
	 * @inheritdoc
	 */
	public function getLastQuery() : string {
		return $this->getLogs()->last();
	}

	/**
	 * Return all sql performed queries as array.
	 * @return Vector sql performed queries.
	 */
	public function getLogs() : Vector {
		return $this->_logs;
	}

	public function getPdoStatement() : PDOStatement {
		return $this->_statement;
	}

	/**
	 * Return PDO Instance
	 * @return PDO PDO Instance
	 */
	public function getPdo() : PDO {
		return $this->_pdo;
	}
}
