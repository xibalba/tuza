<?php
/**
 * @copyright	2014 - 2023 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\builder\traits;

use xibalba\tuza\builder\interfaces\Whereable;

use xibalba\tuza\clause\OrderBy as OrderByClause;
use xibalba\tuza\clause\Where as WhereClause;
use xibalba\tuza\syntax\Factory as FactorySyntax;

use Ds\Vector;

trait WhereAdapter {
	protected WhereClause $_lastWhere;

	protected function _getLastWhere() : WhereClause {
		return $this->_lastWhere;
	}

	protected function _setLastWhere(WhereClause $where) {
		$this->_lastWhere = $where;
	}

	/**
	 * Prepare builder for compose the Where CLAUSE from the OOP API calls.
	 */
	public function where() : Whereable {
		$ss = $this->_getStatement();

		if($ss->hasWhere()) throw new \RuntimeException("the `where()` method must be called once.");
		$ss->setWhere(new WhereClause($ss->getTarget()));

		$this->_setLastWhere($ss->getWhere());

		return $this;
	}

	public function endWhere() : Whereable {
		$this->_setLastWhere($this->_getStatement()->getWhere());
		return $this;
	}

	public function subWhere(string $conjunction) : Whereable {
		$this->_setLastWhere($this->_getStatement()->getWhere()->subWhere($conjunction));
		return $this;
	}

	public function equals(string $column, $value) : Whereable {
		$this->_getLastWhere()->equals($column, $value);
		return $this;
	}

	public function between(mixed $value, mixed $min, mixed $max) : Whereable {
		$this->_getLastWhere()->between($value, $min, $max);
		return $this;
	}

	public function notBetween(mixed $value, mixed $min, mixed $max) : Whereable {
		$this->_getLastWhere()->notBetween($value, $min, $max);
		return $this;
	}

	public function columnBetweenValues(string $column, mixed $min, mixed $max) : Whereable {
		$this->_getLastWhere()->columnBetweenValues($column, $min, $max);
		return $this;
	}

	public function columnNotBetweenValues(string $column, mixed $min, mixed $max) : Whereable {
		$this->_getLastWhere()->columnNotBetweenValues($column, $min, $max);
		return $this;
	}

	public function valueBetweenConlumns(mixed $value, string $columnMin, string $columnMax) : Whereable {
		$this->_getLastWhere()->valueBetweenColumns($value, $columnMin, $columnMax);
		return $this;
	}

	public function valueNotBetweenConlumns(mixed $value, string $columnMin, string $columnMax) : Whereable {
		$this->_getLastWhere()->valueNotBetweenColumns($value, $columnMin, $columnMax);
		return $this;
	}

	public function notEquals(string $column, $value) : Whereable {
		$this->_getLastWhere()->notEquals($column, $value);
		return $this;
	}

	public function greaterThan(string $column, $value) : Whereable {
		$this->_getLastWhere()->greaterThan($column, $value);
		return $this;
	}

	public function greaterThanOrEqual(string $column, $value) : Whereable {
		$this->_getLastWhere()->greaterThanOrEqual($column, $value);
		return $this;
	}

	public function lessThan(string $column, $value) : Whereable {
		$this->_getLastWhere()->lessThan($column, $value);
		return $this;
	}

	public function lessThanOrEqual(string $column, $value) : Whereable {
		$this->_getLastWhere()->lessThanOrEqual($column, $value);
		return $this;
	}

	public function like(string $column, $value) : Whereable {
		$this->_getLastWhere()->like($column, $value);
		return $this;
	}

	public function notLike(string $column, $value) : Whereable {
		$this->_getLastWhere()->notLike($column, $value);
		return $this;
	}

	public function in(string $column, Vector $values) : Whereable {
		$this->_getLastWhere()->in($column, $values);
		return $this;
	}

	public function notIn(string $column, Vector $values) : Whereable {
		$this->_getLastWhere()->notIn($column, $values);
		return $this;
	}

	public function isNull(string $column) : Whereable {
		$this->_getLastWhere()->isNotNull($column);
		return $this;
	}

	public function isNotNull(string $column) : Whereable {
		$this->_getLastWhere()->isNotNull($column);
		return $this;
	}


	public function orderBy($columns, string $direction = OrderByClause::ASC, ?string $target = null) : Whereable {
		if (is_string($columns)) $columns = new Vector([$columns]);
		else if (array_keys($columns) === range(0, count($columns) - 1)) $columns = new Vector($columns);
		else if(!($columns instanceof Vector)) throw new \InvalidArgumentException("You must provide a single column string, or an array or a Vector of strings.");

		$ss = $this->_getStatement();

		$target = is_null($target) ? $ss->getTarget() : FactorySyntax::createTarget($target);

		$ss->orderBy(FactorySyntax::createColumns($columns, $target), $direction);
		return $this;
	}
}