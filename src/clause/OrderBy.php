<?php
/**
 * @copyright	2014 - 2020 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\clause;

use Ds\Vector;
use xibalba\tuza\syntax\Column as ColumnSyntax;

class OrderBy {
	const ASC = "ASC";
	const DESC = "DESC";

	protected $_columns;
	protected $_direction;

	public function __construct(Vector $columns, string $direction = self::ASC) {
		$this->setColumns($columns);
		$this->setDirection($direction);
	}

	public function setColumns(Vector $columns) {
		$this->_columns = $columns;
	}

	public function getColumns() : Vector {
		return $this->_columns;
	}

	public function setDirection(string $direction) {
		switch ($direction) {
			case self::ASC:
			case self::DESC:
				$this->_direction = $direction;
				break;
			default:
				throw new \InvalidArgumentException("Specified direction is not allowed. Only ASC or DESC are allowed.");
		}
	}

	public function getDirection() : string {
		return $this->_direction;
	}
}