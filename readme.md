# Tuza lightweight query builder and database library

Tuza v3 is a lightweight query builder and a API layer over PDO. The main objective of tuza is to provide an an elegant OOP API for complex SQL compositions with PHP.

The SQL composition does not require any database connection, however Tuza also provide a lightweight connection API over PDO.

## 1. Requirement

Tuza require PHP8 or higher and *PHP Data Structures* extension in order to work efficiently, however, for development and testing you can omit to install that extension and Tuza will work transparently with the *PHP Data Structures pollyfill* automatically installed by *Composer*.

## 2. Instalation

The recommended way to install Tuza is trough Composer:

```sh
php composer.phar require xibalba/tuza
```

## 3. Basic query building

Standard SQL's DML consist of four verbs that allow to `SELECT`, `INSERT`, `UPDATE`, and `DELETE` data. Tuza provide a class for each verb with the same name but *camelized*. For a better legibility I recommend assign an alias to those classes to use:

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\builder\Insert as InsertBuilder;
use xibalba\tuza\builder\Update as UpdateBuilder;
use xibalba\tuza\builder\Delete as DeleteBuilder;
```

Tuza's objective is provide the possibility of build queries dynamically and get as result a string with a well formed SQL to be executed (or printed) transparently.

I.e., in order to retrieve all registers and columns from a table called `table_name` we can write this SQL:

```sql
SELECT table_name.* FROM table_name
```

With Tuza we can build that query by:

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\syntax\Column as ColumnSyntax;

$builder = new SelectBuilder();
$builder->from("table_name")
    ->setColumns(ColumnSyntax::ALL);

$sql = $builder->getSql();
echo $sql;
```

The `Select` class can be provided with three optional parameters: a *target*, that is a table or view name, an *alias* for that target, and a schema name. This same parameters can be set when `from()` methods is called.

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\syntax\Column as ColumnSyntax;

// Defining parameters by constructor.
$builderA = new SelectBuilder("the_target", "as_alias", "at_schema");
    ->setColumns(ColumnSyntax::ALL);

// Defining parameters at `from()` method.
$builderB = new SelectBuilder();
$builderB->from("the_target", "as_alias", "at_schema")
    ->setColumns(ColumnSyntax::ALL);

echo $builderA->getSql();
echo $builderB->getSql();
```

Both `$builderA` and `$builderB` build the same result:

```sql
SELECT as_alias.* FROM at_schema.the_target AS as_alias
```

The same way is applied to others builders, i.e., to update some data we can write a query like this:

```sql
UPDATE fake SET str_col = 'text', numeric_col = 6, date_col = :date_val, null_col = NULL
WHERE (fake.bar = :woo) AND (fake.baz > 6)
```

In this example `date_col` and `fake.bar` are linked to some *placeholders* that can be substituted by a concrete value at PDO execution time.

For build this query with Tuza we can employ the `Update` class:

```php
use xibalba\tuza\builder\Update as UpdateBuilder;

// Create an instance providing the target name (table or view)
$uBuilder = new UpdateBuilder("fake");

// Set columns sames and their assosiated values or placeholders.
$uBuilder->set("str_col", "text")
    ->set("numeric_col", 6)
    ->set("date_col", ":date_val")
    ->set("null_col", null);

// Set query conditions.
$uBuilder->where()
    ->equals("bar", ":woo")
    ->greaterThan("baz", 6);

// Get the result SQL
echo $uBuilder->getSql();
```

As you can see, the builder methods return the self instance, so you can concatenate methods calls.

## 4. Brief cookbook

In this brief cookbook I provide some examples for the four query builder classes.

### 4.1 Building `SELECT` statements

#### 4.1.1 Building basic `SELECT` statements

Basic `SELECT` statements are composed defining a list of columns names which data wanted to be retrieved, also the *target* from get that data (a table or view name) and the conditions for that selection. As was shown early, the working table or view, together with its optional alias and schema can be defined both on class constructor or `from()` method. Also, the columns names list are set by the `setColumns()` method. The parameter list provided to `setColumns()` can have so many items as needed.

**Example 1:** basic `SELECT` statement

```php
use xibalba\tuza\builder\Select as SelectBuilder;

$builder = new SelectBuilder();
$builder->from("table_name")
    ->setColumns("column_a", "column_b", "column_c");

echo $builder->getSql();
```

**Result:**

```sql
SELECT table_name.column_a, table_name.column_b, table_name.column_c FROM table_name
```

In order to retrieve all *target* (table or view) columns is used the star `*` character, on Tuza that can be established by the literal or by the defined constant at `syntax\Column::ALL`, that has defined mainly by a legibility reason.

**Example 2:** basic `SELECT` statement with all available columns

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\syntax\Column as ColumnSyntax;

$builderA = new SelectBuilder("table_name");
$builderB = new SelectBuilder("table_name");

$builderA->setColumns("*");
$builderB->setColumns(ColumnSyntax::ALL);

echo $builderA->getSql();
echo $builderB->getSql();
```

**Result:**

```sql
SELECT table_name.* FROM table_name
SELECT table_name.* FROM table_name
```

Both `$builderA` and `$builderB` builds the same result.

#### 4.1.2 Building `SELECT` statements with alias

Many times there is needed to establish an alias not only to tables or views, yet to columns. For that cases, you must employ the `putColumns()` method instead of `setColumns()`. The `putColumns()` method waits a single parameter of key-values pairs `array` or `Ds\Map` type, where *keys* stands for columns names and the values stands for the alias.

**Example 3:** basic `SELECT` statement with alias

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use Ds\Map;

$cMap = new Map();
$cMap->put("col_a", "AL_W");
$cMap->put("col_b", "AL_Y");
$cMap->put("col_c", "AL_Z");

$builderA = new SelectBuilder("table_name", "talias");
$builderB = new SelectBuilder("table_name", "talias");

$builderA->putColumns(["col_a" => "AL_W", "col_b" => "AL_Y", "col_c", => "AL_Z"]);
$builderB->putColumns($cMap);

echo $builderA->getSql();
echo $builderB->getSql();
```

**Result:**

```sql
SELECT talias.col_a AS AL_W, talias.col_b AS AL_Y, talias.col_c AS AL_Z FROM table_name AS talias
```

Both `$builderA` and `$builderB` builds the same result.

#### 4.1.3 Building `SELECT` statements with functions

There are cases that instead of columns what you need to select is a function result. The most common functions are those thah work over a singlre columns, i.e. `MAX()` or `MIN()` to get maximun or minimun value data for the provided column. For those cases Tuza provide the `addFunctionOnColumn()` method, this waits three parameters: (1) a function name, (2) a working column, and (3) a column alias. Sinse the parameters are just strings, you can give any function name that is supported by the target database engine, the only restriction is that function works with a single column name as parameter.

**Example 4:** basic `SELECT` statement with functions

```php
use xibalba\tuza\builder\Select as SelectBuilder;

$sBuilder = new SelectBuilder("table_name");
$sBuilder->addFunctionOnColumn("MAX", "u_id", "total_id");

echo $sBuilder->getSql();
```

**Result:**

```sql
SELECT MAX(table_name.u_id) AS total_id FROM table_name
```

#### 4.1.4 Basic `SELECT` statements with `TOP`, `COUNT` y `LIMIT`

Each database engine provide some clauses for set a limit to the selection results, for example with `TOP` or `LIMIT`, also for counting the results you can use `COUNT`. Tuza provide three methods for each case.

**Example 5:** Basic `SELECT` statement with `TOP`, `COUNT` and `LIMIT`

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\syntax\Column as ColumnSyntax;
use Ds\Map;

$sBuilderCount = new SelectBuilder("table_name");
$sBuilderCount->count("col_name", "calias");

$sBuilderTop = new SelectBuilder("table_name");
$sBuilderTop->setColumns(ColumnSyntax::ALL)->top(9);

$sBuilderLimit = new SelectBuilder("table_name");
$sBuilderLimit->setColumns(ColumnSyntax::ALL)->limit(9);

echo $sBuilder->getSql();
```

**Resultado:**

```sql
SELECT COUNT(table_name.col_name) AS calias FROM table_name
SELECT TOP 9 table_name.* FROM table_name
SELECT table_name.* FROM table_name limit 9
```

#### 4.1.5 Building `Select` statements with conditions

Is usual write `SELECT` queries alongside with a `WHERE` clause of conditions for data selection. The `Select` class of Tuza provide a `where()` method that return the self instance so you can chain conditions methods calls.

**Example 6:** Basic `SELECT` statement with conditions

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use Ds\Map;

$sBuilder = new SelectBuilder();
$sBuilder->from("table_name", "talias")
    ->setColumns("col_a", "col_b", "col_c")
    ->where()
        ->equals("col_x", "foo_bar");

echo $sBuilder->getSql();
```

**Result:**

```sql
SELECT talias.col_a, talias.col_b, talias.col_c FROM table_name AS talias WHERE (talias.col_x = 'foo_bar')
```

Tuza abstract all SQL comparisons operators on analog methods with the same parameter order `{x}("col_name", value)` where `col_name` stand for the columns to compare and `value` stand for the comparison value (except for IS NULL and IS NOT NULL operators that does not require a value).

Next is provided a table with all supported operators:

|SQL Operator |Method                 |Example                            |SQL                            |
|-------------|-----------------------|-----------------------------------|-------------------------------|
|`=`          |`equals()`             |`->equals("col_x", "str")`         |`target.col_x = 'str'`         |
|`<>`         |`notEquals()`          |`->notEquals("col_x", 66)`         |`target.col_x <> 66`           |
|`>`          |`greaterThan()`        |`->greaterThan("col_x", 99)`       |`target.col_x > 99`            |
|`>=`         |`greaterThanOrEqual()` |`->greaterThanOrEqual("col_x", 99)`|`target.col_x >= 99`           |
|`<`          |`lessThan()`           |`->lessThan("col_x", 22)`          |`target.col_x < 22`            |
|`<=`         |`lessThanOrEqual()`    |`->lessThanOrEqual("col_x", 22)`   |`target.col_x <= 99`           |
|`LIKE`       |`like()`               |`->like("col_x", "simil")`         |`target.col_x LIKE 'simil'`    |
|`NOT LIKE`   |`notLike()`            |`->notLike("col_x", "simil")`      |`target.col_x NOT LIKE 'simil'`|
|`IS NULL`    |`isNull()`             |`->isNull("col_x")`                |`target.col_x IS NULL`         |
|`IS NOT NULL`|`isNotNull()`          |`->isNotNull("col_x")`             |`target.col_x IS NOT NULL`     |

The `BETWEEN` and `NOT BETWEEN` operators are a bit different in order to that a minimum and maximum range values must be provided for compare to. Tuza provide six methods for `BETWEEN` comparisons.

##### 4.1.5.1 Compare a columns between values

Tuza provide two methods for compare a columns between values:

1. `columnBetweenValues(string $column, mixed $min, mixed $max)`
2. `columnNotBetweenValues(string $column, mixed $min, mixed $max)`

Where `$column` is a string of a valid `target` column, meanwhile `$min` and `$max` are the minimum and maximum range values for comparte to.

**Example 6.1:** A `SELECT` statement with a `BETWEEN` condition.

```php
use xibalba\tuza\builder\Select as SelectBuilder;

$sBuilder = new SelectBuilder();
$sBuilder->from("table_name")
    ->setColumns("*")
    ->where()
        ->columnBetweenValues("col_x", 66, 99);
```

**Result:**

```sql
SELECT table_name.* FROM table_name WHERE (table_name.col_x BETWEEN 66 AND 99)
```

##### 4.1.5.2 Compare a value between values

Tuza provide two methods for compare values between columns:

1. `valueBetweenColumns(mixed $value, string $columnMin, string $columnMax)`
2. `valueNotBetweenColumns(mixed $value, string $columnMin, string $columnMax)`

Where `$value` is a date string or number, meanwhile `$min` and `$max` are valid columns names of the `$target` established at `SelectBuilder`.

**Example 6.2:** A `SELECT` with a `BETWEEN` condition.

```php
use xibalba\tuza\builder\Select as SelectBuilder;

$sBuilder = new SelectBuilder();
$sBuilder->from("table_name")
    ->setColumns("*")
    ->where()
        ->valueBetweenColumns(66, "col_x", "col_y");
```

**Result:**

```sql
SELECT table_name.* FROM table_name WHERE (66 BETWEEN table_name.col_x AND table_name.col_y)
```

##### 4.1.5.3 Compare a value or column between values or columns

In addition to previous methods, Tuza provide others tew methods tha allow set a columns or value to each argument:

1. `between(mixed $value, mixed $min, mixed $max)`
2. `notBetween(mixed $value, mixed $min, mixed $max)`

To give a value, a string or number must be provided to any of the arguments. To give a column a `syntax\Column` instance must be provided, which one can be easily created with the factory method `createColumn()` of the `syntax\Factory` class.

**Example 6.3:** A `SELECT` with a `BETWEEN` condition

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\syntax\Factory as SyntaxFactory;

$sBuilder = new SelectBuilder();
$sBuilder->from("table_name")
    ->setColumns("*")
    ->where()
        ->between(
            SyntaxFactory::createColumn("foo", null, $target),
            66,
            SyntaxFactory::createColumn( "baz", null, $target),
        )
```

**Result:**

```sql
SELECT table_name.* FROM table_name WHERE (table_name.foo BETWEEN 66 AND table_name.baz)
```

#### 4.1.6 Building ordered `Select` statement

To order ascending or descending a query results you can use the `ORDER BY` clause, which is abstracted by the `orderBy()` method. This method waits as first parameter a column name string, an indexed array or a `Vector` instance with the columns names list to order. By default, the Builder will compose in ascending order, but you can provide a second parameter with the constant `clause\OrderBy::DESC` to indicate a descending order.

**Example 7:** `SELECT` statements with `ORDER BY` clause

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\clause\OrderBy;

$builderSimple = new SelectBuilder("generic");
$builderSimple->setColumns("*")->orderBy("foo");

$builderMultiple = new SelectBuilder("generic");
$builderMultiple->setColumns("*")
    ->orderBy(["foo", "zaz"])
    ->orderBy("baz", OrderBy::DESC);

echo $builderSimple->getSql();
echo $builderMultiple->getSql();
```

**Result:**

```sql
SELECT generic.* FROM generic ORDER BY generic.foo ASC
SELECT generic.* FROM generic ORDER BY generic.foo, generic.zaz ASC, generic.baz DESC
```

#### 4.1.7 Building `SELECT` statements with `JOIN`

The `Select` class support four `JOIN` clauses, each one with it's homonymous method. For the four cases must be provided a target parameter (table or view name) and a columns names list that will be selected from the target. The columns parameter can be an indexed `array` or a `Ds\Vector` instance fulfilled with strings, or a key-value pairs `array`, or a `Ds\Map` instance where each key is a column name string and each value is a wanted alias string. If you want set just some alias for specific columns, then for those that are not needed an alias you must set `null` as value for it correspond key.

Next are shown a example table with tho four supported `join` methods:

|SQL JOIN    |Method        |Example                                                          |
|------------|--------------|-----------------------------------------------------------------|
|`INNER JOIN`|`innerJoin()` |`->innerJoin("target_x", ["col_a", "col_b"])`                    |
|`LEFT JOIN` |`leftJoin()`  |`->leftJoin("target_x", ["col_a" => "c_alias", "col_b" => null])`|
|`RIGHT JOIN`|`rightJoin()` |`->rightJoin("target_x", $vectorInstance)`                       |
|`CROSS JOIN`|`crossJoin()` |`->crossJoin("target_x", $mapInstance)`                          |

Where `$vectorInstance` and `$mapInstance` can be defined as:

```php
use Ds\Vector;
use Ds\Map;

$vectorInstance = new Vector();
$vectorInstance->push("col_a");
$vectorInstance->push("col_b");

$mapInstance = new Map();
$mapInstance->put("col_a", "c_alias");
$mapInstance->put("col_b", null);
```

As is shown, the columns to select from the `JOIN` target are set by it correspond method, not by calling `setColumns()` neither `putColumns()` methods of the `Select` class. The columns set by these methods will be linked exclusively to de target defined at the constructor or the `from()` method. This behavior allows a flexible and dynamic query building, making easier the personalized query building in order to developer needs.

The `JOIN` clause should be alongside with conditions, which are composed on the same manner that the `WHERE` clause. After call some `{x}Join()` method you should call the `on` method in order to prepare the Select builder that calling to comparison methods must be linked to the `JOIN` yet not the `WHERE` clause.

**Example 8:** A `SELECT` statement with `JOIN` clause

```php
use xibalba\tuza\builder\Select as SelectBuilder;

$builder = new SelectBuilder();
$builder->from("user")
    ->putColumns([
        "user_id" => "userId",
        "name" => "username",
        "email" => null,
        "created_at" => null
    ])
    ->leftJoin("news", ["title" => "newsTitle", "body" => null, "updated_at" => null])
        ->on()
        ->equals('author_id', $builder->getColumn('user_id'))
        ->equals('author_id', ":authorId");

$builder->innerJoin("editors")
    ->on()
    ->equals("author_id", $builder->getColumn('user_id'))
    ->equals("author_id", ":authorId")
->where()
    ->lessThan("user_id", ":userId")
    ->notLike("name", ":userNames")
->orderBy("user_id", OrderBy::DESC)
->orderBy("updated_at", OrderBy::DESC, "news");
```

**Result:**

```sql
SELECT user.user_id AS userId, user.name AS username, user.email, user.created_at, news.title AS newsTitle, news.body, news.updated_at FROM user LEFT JOIN news ON (news.author_id = user.user_id) AND (news.author_id = :authorId) INNER JOIN editors ON (editors.author_id = user.user_id) AND (editors.author_id = :authorId) WHERE (user.user_id < :userId) AND (user.name NOT LIKE :userNames) ORDER BY user.user_id DESC, news.updated_at DESC
```

### 4.2 Building `INSERT` statements

The `INSERT` statement is abstracted by the `builder\Insert` class. This class, together with the `Delete` class, are tho most easy working classes. The target must be set at the constructor, optionally you can provide a second parameter with a string for the schema of the target table. The `Insert` class provide different methods in order to allow a flexible way to set columns and data to insert.

#### 4.2.1 Building `INSERT` statement by simple adds

In order to prepare an `INSERT` statement by simple adds, at first you must set the columns list strings name to affect by calling the `setColumns()` method, which waits an arbitrary string list separated by commas. The order of the provided parameters is important since establish the result SQL. After that you can add  values to the builder instance calling the `addValues()` meth od . This method waits an arbitrary values list. As developer you are responsible on guarantee that the data added by `addValues()` match the order and expected type of the columns established by `setColumns()`.

**Example 1:** An `INSERT` statement by simple adds

```php
use xibalba\tuza\builder\Insert as InsertBuilder;

$iBuilder = new InsertBuilder("fake");
$iBuilder->setColumns("str_col", "numeric_col", "date_col", "null_col");
$iBuilder->addValues("str_val", 6.66, '2020-06-01', null);

echo $iBuilder->getSql();
```

**Result:**

```sql
INSERT INTO fake (str_col, numeric_col, date_col, null_col) VALUES ('str_val', 6.66, '2020-06-01', NULL)
```

#### 4.2.2 Building `INSERT` statements by indexed `array`

The `setColumns()` and `addValues()` methods also support receive an indexed `array` as parameter. This mechanism allow to the developer to prepare an `array` with the columns and values and set afterward that `array` to the builder.

**Example 2:** An `INSERT` statement with an indexed `array`

```php
use xibalba\tuza\builder\Insert as InsertBuilder;

$cols = ['str_col', 'num_col', 'bool_col'];
$seeds = [
    ['str_val1', 1.11, false],
    ['str_val2', 2.22, ':bool_2'],
    ['str_val3', 3.33, true]
];

$iBuilder = new InsertBuilder("fake");

$iBuilder->setColumns(...$cols);
foreach ($seeds as $row) $iBuilder->addValues(...$row);

echo $iBuilder->getSql();
```

**Result:**

```sql
INSERT INTO fake (str_col, num_col, bool_col) VALUES ('str_val1', 1.11, 0), ('str_val2', 2.22, :bool_2), ('str_val3', 3.33, 1)
```

#### 4.2.2 Building `INSERT` statements by `putValues()` method

On previous examples was shown how to set first a columns list to affect and afterward the data to insert. There are some cases where you want to insert a single row of data. By the `putValues()` method is possible to set both lists (columns and data) on a single call.

**Example 3:** An `INSERT` statement by `putValues()` on different ways

```php
use xibalba\tuza\builder\Insert as InsertBuilder;
use Ds\Map;

$seed = [
    "str_col" => ":str_val",
    "numeric_col" => 6.66,
    "date_col" => ":date_val",
    "null_col" => null
];

$iMapBuilder = new InsertBuilder("generic");
$iArrBuilder = new InsertBuilder("generic");

$iArrBuilder->putValues($seed);
$iMapBuilder->putValues(new Map($seed));

echo $iArrBuilder->getSql();
echo $iMapBuilder->getSql();
```

**Result:**

```sql
INSERT INTO generic (str_col, numeric_col, date_col, null_col) VALUES (:str_val, 6.66, :date_val, NULL)
```

#### 4.2.3 building `INSERT` statements by the `pushValues()` method

The `Insert` class provide the `pushValues()` method that allow to set values from an indexed `array` of key-value pair arrays or by an `Ds\Vector` instance that constains a list of key-value pair arrays or `Ds\Map` instances. On employ the `pushValues()` method you must consider that the builder will take the columns names from the first set of keys. This mechanism was design for ease the composition of data structures before be set to the builder.

**Example 4:** The `INSERT` stamente building using the `pushValues()` method

```php
use xibalba\tuza\builder\Insert as InsertBuilder;
use Ds\Vector;
use Ds\Map;

$xSeeds = [
    [
        "str_col" => "str_val1",
        "numeric_col" => 1.11,
        "bool_col" => false
    ],
    [
        "str_col" => "str_val2",
        "numeric_col" => 2.22,
        "bool_col" => true
    ],
    [
        "str_col" => "str_val3",
        "numeric_col" => 3.33,
        "bool_col" => true
    ]
];

$vectorArr = new Vector();
$vectorMap = new Vector();

foreach($xSeeds as $s) $vectorArr->push($s);
foreach($xSeeds as $s) $vectorMap->push(new Map($s));

$iArrBuilder = new InsertBuilder("fake");
$iVectorMapBuilder = new InsertBuilder("fake");
$iVectorArrBuilder = new InsertBuilder("fake");

echo $iArrBuilder->getSql();
echo $iVectorMapBuilder->getSql();
echo $iVectorArrBuilder->getSql();
```

**Result:**

```sql
INSERT INTO fake (str_col, numeric_col, bool_col) VALUES ('str_val1', 1.11, 0), ('str_val2', 2.22, 1), ('str_val3', 3.33, 1)
```

The composed SQL by the three exemplified instances (`$iArrBuilder`, `$iVectorMapBuilder` and `$iVectorArrBuilder`) are the same.

### 4.3 Building `UPDATE` statements

The `UPDATE` statement stands for update data on tables, that is usually done in function of some conditions. This sentence is abstracted by the `builder\Update` class. This class provide three different methods for set the columns and data to update, also as the complete API of comparing methods that were shown for the `Select` class in order to compose the sentence conditions.

#### 4.3.1 Building `UPDATE` sentence by `set()` method

The `set()` method waits for two parameters: the first one is tho column name to affect, the second is the value that wants to be updated.

**Example 1:** An `UPDATE` statement with a `set()` method

```php
use xibalba\tuza\builder\Update as UpdateBuilder;

$uBuilder = new UpdateBuilder("fake");
$uBuilder->set("str_col", "text")
    ->set("numeric_col", 6)
    ->set("date_col", ":date_val")
    ->set("null_col", null)
    ->where()
        ->equals("bar", ":woo")
        ->subWhere('OR')
        ->greaterThan("baz", 6)
        ->notLike("zaz", "LAIK");

echo $uBuilder->getSql();
```

**Result:**

```sql
UPDATE fake SET str_col = 'text', numeric_col = 6, date_col = :date_val, null_col = NULL WHERE (fake.bar = :woo) AND ( (fake.baz > 6) OR (fake.zaz NOT LIKE 'LAIK') )
```

As is shown, the `set()` method can be called to established a column-data pair as many times as is needed. Then, calling the `where()` method prepare the builder to set the comparison conditions on the same way as in the `Select` builder class.

#### 4.3.2 Building `UPDATE` statements by `setValues()` method

For those cases where there is a key-value `array`, where each key match for a column name and each value for a data to update or *placeholder* to assign, is a better way to call the `setValues()` method. This method also support a `Ds\Map` instance as parameter.

**Example 2:** An `UPDATE` sentence by the `setValues()` method

```php
use xibalba\tuza\builder\Update as UpdateBuilder;

$seed = [
    "str_col" => "text",
    "numeric_col" => 6,
    "date_col" => ":date_val",
    "null_col" => null
];

$uBuilder = new UpdateBuilder("fake");
$uBuilder->setValues($seed)
    ->equals("bar", ":woo")
    ->subWhere('OR')
        ->greaterThan("baz", 6)
            ->notLike("zaz", "LAIK");

echo $uBuilder->getSql();
```

**Result:**

```sql
UPDATE fake SET str_col = 'text', numeric_col = 6, date_col = :date_val, null_col = NULL WHERE (fake.bar = :woo) AND ( (fake.baz > 6) OR (fake.zaz NOT LIKE 'LAIK') )
```

As is shown, the result is the same that was got at the *Example 1*.

#### 4.3.3 Building `UPDATE` statement by `setMap()` method

As an alternative to the `setValues()` method, the `Update` class provide the `setMap()` method that, as it's name expose, only support as parameter an `Ds\Map` instance as argument.

**Example 3:** An `UPDATE` sentence with `setMap()` method

```php
use xibalba\tuza\builder\Update as UpdateBuilder;
use Ds\Map;

$seed = new Map();
$seed->put("str_col", "text");
$seed->put("numeric_col", 6);
$seed->put("date_col", ":date_val");
$seed->put("null_col", null);

$uBuilder = new UpdateBuilder("fake");
$uBuilder->setMap($seed)
    ->equals("bar", ":woo")
    ->subWhere('OR')
        ->greaterThan("baz", 6)
            ->notLike("zaz", "LAIK");

echo $uBuilder->getSql();
```

**Resultado:**

```sql
UPDATE fake SET str_col = 'text', numeric_col = 6, date_col = :date_val, null_col = NULL WHERE (fake.bar = :woo) AND ( (fake.baz > 6) OR (fake.zaz NOT LIKE 'LAIK') )
```

As on *Example 1* and *Example 2*, the result is the same.

### 4.3 Building `DELETE` statements

The `DELETE` statements removes data from a target table in function of a comparison conditions. This statements is abstracted by the `Delete` class builder. On the same way that the others builder classes, the target table name is set at the constructor, and the conditions are set by calling the comparison methods after calling the `where()` method.

**Example 1:** A `DELETE` sentence

```php
use xibalba\tuza\builder\Delete as DeleteBuilder;

$dBuilder = new DeleteBuilder("fake");
$dBuilder->where()
    ->equals("bar", ":woo")
    ->subWhere('OR')
        ->greaterThan("baz", 6)
        ->notLike("zaz", "LAIK");

echo $dBuilder->getSql();
```

**Result:**

```sql
DELETE FROM fake  WHERE (fake.bar = :woo) AND ( (fake.baz > 6) OR (fake.zaz NOT LIKE 'LAIK') )
```
