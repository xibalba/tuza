<?php
/**
 * @copyright	2014 - 2023 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\syntax;

use xibalba\tuza\syntax\Target as TargetSyntax;
use xibalba\tuza\syntax\traits\TargetAware;

/**
 * [Syntax abstraction] Class Column
 * This class abstracts a `COLUMN` syntax composition.
 * Consider that this class only abstract the syntax construction, not the
 * SQL structure, so this class do not compose attributes like constrains, instead,
 * any syntax element that employ a column reference will require a instance of this class.
 *
 * @package xibalba\tuza\syntax
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Column {
	use TargetAware;

	const ALL = "*";

	private string $__name;
	private ?string $__alias = null;

	public function __construct(string $name, TargetSyntax $target, ?string $alias = null) {
		$this->setName($name);
		$this->setTarget($target);
		if(!is_null($alias)) $this->setAlias($alias);
	}

	/**
	 * @return string
	 */
	public function getName() : string {
		return $this->__name;
	}

	/**
	 * @param string $name
	 */
	public function setName(string $name) {
		$this->__name = $name;
	}

	/**
	 * @return string
	 */
	public function getAlias() : string {
		return is_null($this->__alias) ? "" : $this->__alias;
	}

	/**
	 * @param string $alias
	 */
	public function setAlias(string $alias) {
		if($this->isAll()) throw new \RuntimeException("Cannot use alias because column name is ALL (*)");
		$this->__alias = $alias;
	}

	/**
	 * @return bool
	 */
	public function hasAlias() : bool {
		return !empty($this->__alias);
	}

	/**
	 * Return a boolean value whether the column name is or nor an ALL mark.
	 * @return boolean
	 */
	public function isAll() : bool {
		return $this->getName() === self::ALL;
	}
}
