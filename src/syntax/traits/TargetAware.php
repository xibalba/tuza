<?php
/**
 * @copyright	2014 - 2020 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\syntax\traits;

use xibalba\tuza\syntax\Target as TargetSyntax;

/**
 * Provide methods for manage a syntax target FROM instance
 *
 * @package xibalba\tuza\syntax
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
trait TargetAware {
	protected ?TargetSyntax $_target = null;

	/**
	 * @return TargetSyntax
	 */
	public function getTarget() : TargetSyntax {
		return $this->_target;
	}

	/**
	 * @param TargetSyntax $target
	 */
	public function setTarget(TargetSyntax $target) {
		$this->_target = $target;
	}

	public function hasTarget() : bool {
		return $this->_target instanceof TargetSyntax;
	}
}
