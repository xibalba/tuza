<?php

namespace guatuza;

use PHPUnit\Framework\TestCase;

use xibalba\tuza\builder\Select as SelectBuilder;

use xibalba\tuza\clause\OrderBy;

use xibalba\tuza\syntax\Column as ColumnSyntax;

use \Ds\Map;

class SelectBuilderTest extends TestCase {
	public function testSimple() {
		$expected = "SELECT fake.* FROM fake";

		$sBuilder = new SelectBuilder("fake");

		// Call setColumns() for a parameters list of strings
		$query = $sBuilder->setColumns(ColumnSyntax::ALL)
			->getSql();

		$this->assertSame($expected, $query);
	}

	public function testVerbose() {
		$expected = "SELECT generic.baz, generic.bar, generic.queso FROM generic";

		$sBuilder = new SelectBuilder();
		$query = $sBuilder->from("generic")
			->setColumns("baz", "bar", "queso")
			->getSql();

		$this->assertSame($expected, $query);
	}

	public function testDistinct() {
		$expected = "SELECT DISTINCT generic.baz, generic.bar, generic.queso FROM generic";

		$sBuilder = new SelectBuilder();
		$query = $sBuilder->distinct()
			->from("generic")
			->setColumns("baz", "bar", "queso")
			->getSql();

		$this->assertSame($expected, $query);
	}

	public function testWithTargetAlias() {
		$expected = "SELECT aliado.baz, aliado.bar, aliado.queso FROM sch.generic AS aliado";

		$sBuilder = new SelectBuilder("generic", "aliado", "sch");
		
		$query = $sBuilder->setColumns("baz", "bar", "queso")
			->getSql();

		$this->assertSame($expected, $query);
	}

	public function testWithColumnAlias() {
		$expected = "SELECT sch.generic.baz AS __BAR__, sch.generic.bar AS FOO FROM sch.generic";

		$sBuilder = new SelectBuilder();
		$sBuilder->from(target: "generic", schema: "sch");

		// Call to putColumns for a Map of columns where the key is the column and
		// the value is the alias
		$sBuilder->putColumns(new Map(["baz" => "__BAR__", "bar" => "FOO"]));

		$this->assertSame($expected, $sBuilder->getSql());
	}

	public function testWithSchemaAndAlias() {
		$expected = "SELECT talias.baz AS __BAR__, talias.bar AS FOO FROM sch.generic AS talias";

		$sBuilder = new SelectBuilder();
		$sBuilder->from("generic", "talias", "sch");

		// putColumns() also must support provide an indexed array
		$sBuilder->putColumns(["baz" => "__BAR__", "bar" => "FOO"]);

		$this->assertSame($expected, $sBuilder->getSql());
	}

	public function testEquals() {
		$expected = "SELECT aliado.baz, aliado.bar, aliado.queso FROM sch.generic AS aliado WHERE (aliado.bar = 'foo')";

		$sBuilder = new SelectBuilder();
		$sBuilder->from("generic", "aliado", "sch")
			->setColumns("baz", "bar", "queso")
			->where()->equals("bar", "foo");

		$this->assertSame($expected, $sBuilder->getSql());
	}

	public function testBasicInjection() {
		$expected = "SELECT generic.baz, generic.bar FROM generic WHERE (generic.bar = '1 OR 1=1')";

		$sBuilder = new SelectBuilder("generic");
		$sBuilder->setColumns("baz", "bar")
			->where()->equals("bar", "1 OR 1=1");

		$this->assertSame($expected, $sBuilder->getSql());
	}

	public function testPersonalPlaceholder() {
		$expected = "SELECT generic.baz, generic.bar FROM generic WHERE (generic.bar = :woo)";

		$sBuilder = new SelectBuilder();
		$sBuilder->from("generic")
			->setColumns("baz", "bar")
			->where()->equals("bar", ":woo");

		$this->assertSame($expected, $sBuilder->getSql());
	}

	public function testComparisons() {
		$expected = "SELECT generic.baz, generic.bar FROM generic WHERE (generic.bar = :woo) AND (generic.baz > 6) AND (generic.zaz NOT LIKE 'LAIK')";

		$sBuilder = new SelectBuilder("generic");
		$sBuilder->setColumns("baz", "bar")
			->where()->equals("bar", ":woo")
				->greaterThan("baz", 6)
				->notLike("zaz", "LAIK");

		$this->assertSame($expected, $sBuilder->getSql());
	}

	public function testComparisonsComplexOne() {
		$expected = "SELECT generic.baz, generic.bar FROM generic WHERE (generic.bar = :woo) AND ( (generic.baz > 6) OR (generic.zaz NOT LIKE 'LAIK') )";

		$sBuilder = new SelectBuilder("generic");
		$sBuilder->setColumns("baz", "bar")
			->where()
				->equals("bar", ":woo")
				->subWhere('OR')
					->greaterThan("baz", 6)
					->notLike("zaz", "LAIK");

		$this->assertSame($expected, $sBuilder->getSql());
	}

	public function testFunctions() {
		$expected = "SELECT fake.baz, MAX(fake.u_id) AS total_id FROM fake";

		$sBuilder = new SelectBuilder("fake");
		$sBuilder->setColumns("baz")
			->addFunctionOnColumn("MAX", "u_id", "total_id");

		$this->assertSame($expected, $sBuilder->getSql());
	}

	public function testTop() {
		$expected = "SELECT TOP 9 fake.* FROM fake";

		$sBuilder = new SelectBuilder("fake");

		// Call setColumns() for a parameters list of strings
		$query = $sBuilder->setColumns(ColumnSyntax::ALL)
			->top(9)
			->getSql();

		$this->assertSame($expected, $query);
	}

	public function testCount() {
		$expectedAll = "SELECT COUNT(generic.*) FROM generic";
		$expectedCol = "SELECT COUNT(generic.foo) FROM generic";
		$expectedAs = "SELECT COUNT(generic.foo) AS aliased FROM generic";

		$builderAll = new SelectBuilder();
		$builderCol = new SelectBuilder();
		$builderAs = new SelectBuilder();

		$queryAll = $builderAll->from('generic')->count()->getSql();
		$queryCol = $builderCol->from('generic')->count('foo')->getSql();
		$queryAs = $builderAs->from('generic')->count('foo', 'aliased')->getSql();

		$this->assertSame($expectedAll, $queryAll);
		$this->assertSame($expectedCol, $queryCol);
		$this->assertSame($expectedAs, $queryAs);
	}

	public function testOrderBy() {
		$expectedSingle = "SELECT generic.* FROM generic ORDER BY generic.foo ASC";
		$expectedMultiple = "SELECT generic.* FROM generic ORDER BY generic.foo, generic.zaz ASC, generic.baz DESC";

		$builderSimple = new SelectBuilder();
		$builderMultiple = new SelectBuilder();

		$builderSimple->from("generic")
			->setColumns("*")
			->orderBy("foo");

		$builderMultiple->from("generic")
			->setColumns("*")
			->orderBy(["foo", "zaz"])
			->orderBy("baz", OrderBy::DESC);

		$this->assertSame($expectedSingle, $builderSimple->getSql());
		$this->assertSame($expectedMultiple, $builderMultiple->getSql());
	}

	public function testJoin() {
		$expected = "SELECT generic.* FROM generic "
			. "INNER JOIN news ON (news.author_id = generic.user_id) AND (news.author_id = 1)";

		$builder = new SelectBuilder("generic");
		$builder->setColumns("*")
			->innerJoin("news")
			->on()
				->equals('author_id', $builder->getColumn('user_id'))
				->equals('author_id', 1);

		$query = $builder->getSql();

		$this->assertSame($expected, $query);
	}

	public function testMultiplesJoin() {
		$expected = "SELECT "
			. "user.user_id AS userId, "
			. "user.name AS username, "
			. "user.email, "
			. "user.created_at, "
			. "news.title AS newsTitle, "
			. "news.body, "
			. "news.updated_at "
		. "FROM user "
			. "LEFT JOIN news ON (news.author_id = user.user_id) AND (news.author_id = :authorId) "
			. "INNER JOIN editors ON (editors.author_id = user.user_id) AND (editors.author_id = :authorId) "

			. "WHERE (user.user_id < :userId) AND (user.name NOT LIKE :userNames) "
			. "ORDER BY user.user_id DESC, news.updated_at DESC";

		$builder = new SelectBuilder();
		$builder->from("user")
			->putColumns([
				"user_id" => "userId",
				"name" => "username",
				"email" => null,
				"created_at" => null
			])
			->leftJoin("news", ["title" => "newsTitle", "body" => null, "updated_at" => null])
				->on()
				->equals('author_id', $builder->getColumn('user_id'))
				->equals('author_id', ":authorId");

		$builder->innerJoin("editors")
			->on()
			->equals("author_id", $builder->getColumn('user_id'))
			->equals("author_id", ":authorId")
		->where()
			->lessThan("user_id", ":userId")
			->notLike("name", ":userNames")
		->orderBy("user_id", OrderBy::DESC)
		->orderBy("updated_at", OrderBy::DESC, "news");

		$this->assertSame($expected, $builder->getSql());
	}

	public function testLimit() {
		$expected = "SELECT fake.* FROM fake LIMIT 9";

		$sBuilder = new SelectBuilder("fake");

		// Call setColumns() for a parameters list of strings
		$query = $sBuilder->setColumns(ColumnSyntax::ALL)
			->limit(9)
			->getSql();

		$this->assertSame($expected, $query);
	}

	public function testOffset() {
		$expected = "SELECT fake.* FROM fake LIMIT 9 OFFSET 6";

		$sBuilder = new SelectBuilder("fake");

		// Call setColumns() for a parameters list of strings
		$query = $sBuilder->setColumns(ColumnSyntax::ALL)
			->limit(9, 6)
			->getSql();

		$this->assertSame($expected, $query);
	}
}
