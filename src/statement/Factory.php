<?php
/**
 * @copyright	2014 - 2019 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\statement;

use xibalba\tuza\expression\Factory as ExpressionFactory;
use xibalba\tuza\syntax\Column as ColumnSyntax;
use xibalba\tuza\syntax\Factory as SyntaxFactory;

use xibalba\tuza\statement\Select as SelectStatement;

use \Ds\Vector;
use \Ds\Map;

class Factory {
	public static function createSelect(?string $target = null) : SelectStatement {
		$ss = new SelectStatement();
		if(!is_null($target)) $ss->setTarget(SyntaxFactory::createTarget($target));
		return $ss;
	}
}
