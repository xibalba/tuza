<?php
/**
 * @copyright	2014 - 2021 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\clause\dialects;

/**
 * Class Top
 * This class abstract a dialectic TOP clause.
 * This clause is supported only by MS-SQL
 * @package xibalba\tuza\clause\dialects
 */
class Top {
	private $__top;

	public function __construct(int $top) {
		$this->setTop($top);
	}

	public function setTop(int $top) {
		if($top <= 0) throw new \InvalidArgumentException("TOP value must be equal or grater than one.");
		$this->__top = $top;
	}

	public function getTop() : int {
		return $this->__top;
	}
}