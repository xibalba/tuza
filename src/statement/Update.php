<?php
/**
 * @copyright	2014 - 2024 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\statement;

use xibalba\tuza\syntax\traits\TargetAware;
use xibalba\tuza\clause\traits\WhereAware;

use xibalba\tuza\syntax\Factory as FactorySyntax;

use \Ds\Map;

class Update {
	use TargetAware, WhereAware;

	protected Map $_pairs;

	public function __construct() {
		$this->_pairs = new Map();
	}

	public function set(string $colName, $value) {
		$column = FactorySyntax::createColumn($colName, null, $this->getTarget());
		$this->getPairs()->put($column, $value);
	}

	public function setPairs(Map $pairs) {
		$this->getPairs()->clear();
		foreach ($pairs as $key => $value) $this->set($key, $value);
	}

	public function getPairs() : Map {
		return $this->_pairs;
	}
}