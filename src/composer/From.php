<?php

/**
 * @copyright	2014 - 2019 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\composer;

use xibalba\tuza\composer\Columns as ColumnsComposer;

use xibalba\tuza\statement\Select as SelectStatement;
use xibalba\tuza\syntax\Target as TargetSyntax;

use \Ds\Vector;

/**
 * This class provide an OOP API for query building.
 *
 * @package xibalba\tuza\syntax
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class From {
	public static function composeWithAlias(TargetSyntax $target) : string {
		return $target->hasAlias()
			? static::composeWithSchema($target) . " AS " . $target->getAlias()
			: static::composeWithSchema($target);
	}

	public static function composeWithSchema(TargetSyntax $target) : string {
		return $target->getQualifiedName();
	}
}
