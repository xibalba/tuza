<?php
/**
 * @copyright	2014 - 2021 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza;

use \PDOStatement;

/**
 * DbConnectionInterface define the interface that should be implemented by
 * any class that represent a Database Connection.
 * Tuza implement this by `DbConnection` class, but you can implement your own class or
 * extend DbConnection for your own needs.
 * By this way you have a defined structure to build database connections but a complete
 * freedom about the implementation.
 *
 * @package xibalba\tuza
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
interface DbConnectionInterface {
	/**
	 * Execute an SQL statement and return the `\PDOStatement` created object.
	 * If fail then Rise a \DbException.
	 *
	 * @param string $query
	 * @return PDOStatement Statement object created for execution.
	 */
	public function exec(string $query) : PDOStatement;

	/**
	 * Return the last performed query.
	 */
	public function getLastQuery() : string;
}
