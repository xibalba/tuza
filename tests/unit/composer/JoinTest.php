<?php

namespace guatuza\composer;

use PHPUnit\Framework\TestCase;

use xibalba\tuza\clause\Join as JoinClause;
use xibalba\tuza\syntax\Column as ColumnSyntax;
use xibalba\tuza\syntax\Target as TargetSyntax;

use xibalba\tuza\composer\Join as JoinComposer;
use xibalba\tuza\syntax\Factory as SyntaxFactory;

use Ds\Map;
use Ds\Vector;

class JoinTest extends TestCase {
	public function testOnSimple() {
		$expected = "INNER JOIN news ON (news.author_id = user.user_id) AND (news.author_id = 1)";

		$table = new TargetSyntax("news");
		$join = new JoinClause($table, JoinClause::JOIN_INNER);

		$join->on()
			->equals("author_id", new ColumnSyntax("user_id", new TargetSyntax("user")))
			->equals("author_id", 1);

		$this->assertSame($expected, JoinComposer::compose($join));
	}
}