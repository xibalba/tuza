<?php
/**
 * @copyright	2014 - 2024 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\statement;

use xibalba\tuza\expression\traits\ColumnsAware as ColumnsExpressionAware;

use \Ds\Vector;

class Insert {
	use ColumnsExpressionAware;

	protected Vector $_values;

	public function __construct() {
		$this->_values = new Vector();
	}

	public function pushValues(Vector $values) {
		if($this->getColumnsExpression()->getColumns()->count() === $values->count()) {
			$this->_values->push($values);
		}
		else throw new \InvalidArgumentException("Values quantity and columns does not match.");
	}

	public function getValues() : Vector {
		return $this->_values;
	}
}