<?php
/**
 * @copyright	2014 - 2024 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\syntax;

use xibalba\tuza\syntax\Target as TargetSyntax;

use \Ds\Vector;
use \Ds\Map;

/**
 * This class implement the Factory design pattern in order to provide an API
 * for an easy object building.
 */
class Factory {
	public static function createTarget(string $name, string $alias = "", string $schema = "") : TargetSyntax {
		return new TargetSyntax($name, $alias, $schema);
	}

	public static function createColumn(string $columnName, ?string $alias, TargetSyntax $target) : Column {
		return new Column($columnName, $target, $alias);
	}

	public static function createColumnsWithAlias(Map $columns, TargetSyntax $target) : Vector {
		$cols = new Vector();

		foreach ($columns as $coll => $alias) {
			$cols->push(static::createColumn($coll, $alias, $target));
		}

		return $cols;
	}

	public static function createColumns(Vector $columns, TargetSyntax $target) : Vector {
		$cols = new Vector();

		foreach ($columns as $coll) {
			$cols->push(static::createColumn($coll, null, $target));
		}

		return $cols;
	}
}
