<?php
/**
 * @copyright	2014 - 2021 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\clause\dialects;

/**
 * Class Limit
 * This class abstract a dialectic LIMIT CLAUSE.
 * MariaDB, MySQL and SQLIte support thsi clause. An class that
 * abstracts a SELECT Statement supporting those dialects
 * must implement this class.
 *
 * @package xibalba\tuza\clause\dialects
 */
class Limit {
	private $__limit;
	private $__offset;

	public function __construct(int $limit, ?int $offset) {
		$this->setLimit($limit);
		if(!is_null($offset)) $this->setOffset($offset);
		else $this->__offset = null;
	}

	public function setLimit(int $limit) {
		if($limit <= 0) throw new \InvalidArgumentException("Limit value must be equal or grater than one.");
		$this->__limit = $limit;
	}

	public function setOffset(?int $offset) {
		if(is_int($offset) && $offset <= 0) throw new \InvalidArgumentException("Offset value must be equal or grater than one.");
		$this->__offset = $offset;
	}

	public function hasOffset() : bool {
		return !is_null($this->__offset);
	}

	public function getLimit() : int {
		return $this->__limit;
	}

	public function getOffset() : ?int {
		return $this->__offset;
	}
}
