<?php
/**
 * @copyright	2014 - 2020 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\clause\traits;

use xibalba\tuza\clause\Where as WhereClause;

use \Ds\Vector;

trait WhereAware {

	protected ?WhereClause $_whereClause = null;
	protected ?Vector $_addedWheres = null;

	public function setWhere(WhereClause $where) {
		$this->_whereClause = $where;
	}

	public function getWhere() : WhereClause {
		return $this->_whereClause;
	}

	public function addWhere(WhereClause $where) {
		if(is_null($this->_addedWheres)) $this->_addedWheres = new Vector();
		$this->_addedWheres->push($where);
	}

	public function hasWhere() {
		return $this->_whereClause instanceof WhereClause;
	}
}