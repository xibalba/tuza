<?php

/**
 * @copyright    2014 - 2020 Xibalba Lab.
 * @license      http://opensource.org/licenses/MIT
 * @link         https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\composer;

use xibalba\tuza\clause\Join as JoinClause;

use xibalba\tuza\syntax\Column as ColumnSyntax;

use xibalba\tuza\composer\Columns as ColumnComposer;

use xibalba\tuza\composer\Placeholder as PlaceholderComposer;
use xibalba\tuza\composer\From as FromComposer;
use xibalba\tuza\composer\Where as WhereComposer;

use \Ds\Map;
use \Ds\Vector;

class Join {
	public static function compose(JoinClause $join) : string {
		return $join->getType()
			. " JOIN "
			. FromComposer::composeWithAlias($join->getTarget())
			. " ON "
			. WhereComposer::compose($join->getWhere());
	}
}