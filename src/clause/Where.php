<?php
/**
 * @copyright	2014 - 2023 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\clause;

use xibalba\tuza\syntax\traits\TargetAware;
use xibalba\tuza\syntax\Factory as SyntaxFactory;

use xibalba\tuza\syntax\Target as TargetSyntax;

use \Ds\Map;
use \Ds\Vector;

/**
 * This class is an abstraction that wraps a SQL WHERE clause.
 *
 * @package xibalba\tuza\syntax
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Where {
	use TargetAware;

	const OPERATOR_GREATER_THAN_OR_EQUAL = ">=";
	const OPERATOR_GREATER_THAN = ">";
	const OPERATOR_LESS_THAN_OR_EQUAL = "<=";
	const OPERATOR_LESS_THAN = "<";

	const OPERATOR_LIKE = "LIKE";
	const OPERATOR_NOT_LIKE = "NOT LIKE";

	const OPERATOR_IS = "IS";
	const OPERATOR_IS_NOT = "IS NOT";

	const OPERATOR_EQUAL = "=";
	const OPERATOR_NOT_EQUAL = "<>";
	const OPERATOR_BETWEEN = "BETWEEN";
	const OPERATOR_NOT_BETWEEN = "NOT BETWEEN";

	const CONJUNCTION_AND = "AND";
	const CONJUNCTION_AND_NOT = "AND NOT";
	const CONJUNCTION_OR = "OR";
	const CONJUNCTION_OR_NOT = "OR NOT";
	const CONJUNCTION_EXISTS = "EXISTS";
	const CONJUNCTION_NOT_EXISTS = "NOT EXISTS";

	protected string $_conjunction;

	protected Vector $_comparisons;
	protected Vector $_betweens;
	protected Map $_ins;
	protected Map $_notIns;

	protected Vector $_subWheres;

	/**
	 * Where constructor.
	 * A Where clause representation demands for a related table and a sepecific conjunction.
	 *
	 * @param TargetSyntax $target
	 * @param string $conjunction
	 */
	public function __construct(TargetSyntax $target, string $conjunction = self::CONJUNCTION_AND) {
		$this->setTarget($target);
		$this->setConjunction($conjunction);

		$this->_comparisons = new Vector();
		$this->_betweens = new Vector();
		$this->_subWheres = new Vector();

		$this->_ins = new Map();
		$this->_notIns = new Map();
	}

	/**
	 * Adds a new comparison to the comparisons Vector. Most of comparisons consists on a subject column,
	 * a conjunction operator and a target value. User must use public comparisons methods in order to
	 * ensure a suppoerted proper operator.
	 *
	 * @param string $column
	 * @param mixed $value
	 * @param string $operator
	 * @return $this
	 */
	protected function _addComparison(string $column, mixed $value, string $operator) : Where {
		$comparison = new Map();

		$comparison->put("subject", SyntaxFactory::createColumn($column, null, $this->getTarget()));
		$comparison->put("conjunction", $operator);
		$comparison->put("target", $value);

		$this->_comparisons->push($comparison);

		return $this;
	}

	/**
	 * Adds a BETWEEN comparison to betweens Vector. A BETWEEN comparison consist on a subject column or value,
	 * the BETWEEN or NOT BETWEEN operator, and the minimal and maximun range values for compare to.
	 *
	 * @param string $operator
	 * @param mixed $subject
	 * @param mixed $min
	 * @param mixed $max
	 * @return void
	 */
	public function _addBetween(string $operator, mixed $subject, mixed $min, mixed $max) : void {
		$between = new Map();

		$between->put("subject", $subject);
		$between->put("operator", $operator);
		$between->put("min", $min);
		$between->put("max", $max);

		$this->_betweens->push($between);
	}

	/**
	 * Set the conjunction operator for the Where instance. There is supported onlyh four operators:
	 * AND, OR, AND NOT, & OR NOT.
	 *
	 * @param string $operator
	 */
	public function setConjunction(string $operator) {
		if (false === in_array(
				$operator,
				[self::CONJUNCTION_AND, self::CONJUNCTION_OR, self::CONJUNCTION_OR_NOT, self::CONJUNCTION_AND_NOT]
			)
		) {
			throw new \InvalidArgumentException(
				"Invalid conjunction specified, must be one of AND or OR, but '" . $operator . "' was found."
			);
		}

		$this->_conjunction = $operator;
	}

	/**
	 * return the current conjunction operator.
	 * @return string
	 */
	public function getConjunction() : string {
		return $this->_conjunction;
	}

	/**
	 * A WHERE clause support the addition of "sub WHERES". Usually a sub where works with a diferent
	 * operator that the outside WHERE.
	 *
	 * @param string $operator
	 * @return Where
	 */
	public function subWhere(string $operator) : Where {
		$subWhere = new Where($this->getTarget(), $operator);
		$this->_subWheres->push($subWhere);
		return $subWhere;
	}

	/**
	 * Return the collection of sub wheres, if any, of the actual instance.
	 * @return Vector
	 */
	public function getSubWheres() : Vector {
		return $this->_subWheres;
	}

	/**
	 * Add an equals comparisons between a column and a value. V.g.: `some_column = some_value`
	 *
	 * @param string $column
	 * @param $value
	 * @return Where
	 */
	public function equals(string $column, $value) : Where {
		return $this->_addComparison($column, $value, self::OPERATOR_EQUAL);
	}

	/**
	 * Add a BETWEEN comparison for a subject and range min and max values.
	 * Each argument can be provided with Column instances, so user can compose the comparison as wish.
	 *
	 * @param mixed $value
	 * @param mixed $min
	 * @param mixed $max
	 * @return $this
	 */
	public function between(mixed $value, mixed $min, mixed $max) : Where {
		$this->_addBetween(self::OPERATOR_BETWEEN, $value, $min, $max);
		return $this;
	}

	/**
	 * Add a NOT BETWEEN comparison for a subject and range min and max values.
	 * Each argument can be provided with Column instances, so user can compose the comparison as wish.
	 *
	 * @param mixed $value
	 * @param mixed $min
	 * @param mixed $max
	 * @return $this
	 */
	public function notBetween(mixed $value, mixed $min, mixed $max) : Where {
		$this->_addBetween(self::OPERATOR_NOT_BETWEEN, $value, $min, $max);
		return $this;
	}

	/**
	 * Add a BETWEEN comparison for a column subject to be compared against min and max values range.
	 *
	 * @param string $column
	 * @param mixed $min
	 * @param mixed $max
	 * @return $this
	 */
	public function columnBetweenValues(string $column, mixed $min, mixed $max) : Where {
		$this->_addBetween(
			self::OPERATOR_BETWEEN,
			SyntaxFactory::createColumn($column, null, $this->getTarget()),
			$min,
			$max
		);

		return $this;
	}

	/**
	 * Add a NOT BETWEEN comparison for a column subject to be compared against min and max values range.
	 *
	 * @param string $column
	 * @param mixed $min
	 * @param mixed $max
	 * @return $this
	 */
	public function columnNotBetweenValues(string $column, mixed $min, mixed $max) : Where {
		$this->_addBetween(
			self::OPERATOR_NOT_BETWEEN,
			SyntaxFactory::createColumn($column, null, $this->getTarget()),
			$min,
			$max
		);

		return $this;
	}

	/**
	 * Add a BETWEEN comparison for a value subject to be compared against min and max columns values range.
	 *
	 * @param mixed $value
	 * @param string $columnMin
	 * @param string $columnMax
	 * @return $this
	 */
	public function valueBetweenColumns(mixed $value, string $columnMin, string $columnMax) : Where {
		$this->_addBetween(
			self::OPERATOR_BETWEEN,
			$value,
			SyntaxFactory::createColumn($columnMin, null, $this->getTarget()),
			SyntaxFactory::createColumn($columnMax, null, $this->getTarget())
		);

		return $this;
	}

	/**
	 * Add a NOT BETWEEN comparison for a value subject to be compared against min and max columns values range.
	 *
	 * @param mixed $value
	 * @param string $columnMin
	 * @param string $columnMax
	 * @return $this
	 */
	public function valueNotBetweenColumns(mixed $value, string $columnMin, string $columnMax) : Where {
		$this->_addBetween(
			self::OPERATOR_NOT_BETWEEN,
			$value,
			SyntaxFactory::createColumn($columnMin, null, $this->getTarget()),
			SyntaxFactory::createColumn($columnMax, null, $this->getTarget())
		);

		return $this;
	}

	/**
	 * Add a not equals comparisons between a column and a value. V.g.: `some_column <> some_value`
	 *
	 * @param string $column
	 * @param $value
	 * @return Where
	 */
	public function notEquals(string $column, $value) : Where {
		return $this->_addComparison($column, $value, self::OPERATOR_NOT_EQUAL);
	}

	/**
	 * Add a greater than comparisons between a column and a value. V.g.: `some_column > some_value`
	 *
	 * @param string $column
	 * @param $value
	 * @return Where
	 */
	public function greaterThan(string $column, $value) : Where {
		return $this->_addComparison($column, $value, self::OPERATOR_GREATER_THAN);
	}

	/**
	 * Add a greater than or equals comparisons between a column and a value. V.g.: `some_column >= some_value`
	 *
	 * @param string $column
	 * @param $value
	 * @return Where
	 */
	public function greaterThanOrEqual(string $column, $value) : Where {
		return $this->_addComparison($column, $value, self::OPERATOR_GREATER_THAN_OR_EQUAL);
	}

	/**
	 * Add a less comparisons between a column and a value. V.g.: `some_column < some_value`
	 *
	 * @param string $column
	 * @param $value
	 * @return Where
	 */
	public function lessThan(string $column, $value) : Where {
		return $this->_addComparison($column, $value, self::OPERATOR_LESS_THAN);
	}

	/**
	 * Add a less than or equals comparisons between a column and a value. V.g.: `some_column <= some_value`
	 *
	 * @param string $column
	 * @param $value
	 * @return Where
	 */
	public function lessThanOrEqual(string $column, $value) : Where {
		return $this->_addComparison($column, $value, self::OPERATOR_LESS_THAN_OR_EQUAL);
	}

	/**
	 * Add a like comparisons between a column and a value. V.g.: `some_column LIKE some_value`
	 *
	 * @param string $column
	 * @param $value
	 * @return Where
	 */
	public function like(string $column, $value) : Where {
		return $this->_addComparison($column, $value, self::OPERATOR_LIKE);
	}

	/**
	 * Add a not like comparisons between a column and a value. V.g.: `some_column NOT LIKE some_value`
	 *
	 * @param string $column
	 * @param $value
	 * @return Where
	 */
	public function notLike(string $column, $value) : Where {
		return $this->_addComparison($column, $value, self::OPERATOR_NOT_LIKE);
	}

	public function in(string $column, Vector $values) : Where {
		$this->_ins->put(SyntaxFactory::createColumn($column, null, $this->getTarget()), $values);
		return $this;
	}

	public function notIn(string $column, Vector $values) : Where {
		$this->_notIns->put(SyntaxFactory::createColumn($column, null, $this->getTarget()), $values);
		return $this;
	}

	public function isNull(string $column) : Where {
		return $this->_addComparison($column, null, self::OPERATOR_IS);
	}

	public function isNotNull(string $column) : Where {
		return $this->_addComparison($column, null, self::OPERATOR_IS_NOT);
	}

	/**
	 * @return Vector
	 */
	public function getComparisons() : Vector {
		return $this->_comparisons;
	}

	public function getBetweens() : Vector {
		return $this->_betweens;
	}

	/**
	 * @return Map
	 */
	public function getIns() : Map {
		return $this->_ins;
	}

	/**
	 * @return Map
	 */
	public function getNotIns() : Map {
		return $this->_notIns;
	}
}
