<?php
/**
 * @copyright	2014 - 2021 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza;

/**
 * DbConnectable define the minimal interface that must be implemented by
 * classes that require a DbConnectionInterface kind object.
 * A single connection object must be shared between all class instance, so
 * the defined methods are static.
 *
 * @package xibalba\tuza
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
interface DbConnectable {
	/**
	 * Return the actual connection instance.
	 *
	 * @return DbConnectionInterface The actual connection instance
	 */
	public static function getDbConnection() : DbConnectionInterface;
}
