<?php

/**
 * @copyright	2014 - 2020 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\statement\dialects;

use xibalba\tuza\clause\dialects\Limit as LimitClause;

use xibalba\tuza\statement\Select;

class LimitSelect extends Select {
	protected $_limitClause;
}