<?php
/**
 * @copyright	2014 - 2020 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\composer;

use xibalba\tuza\composer\From as FromComposer;
use xibalba\tuza\composer\Columns as ColumnsComposer;
use xibalba\tuza\composer\Where as WhereComposer;
use xibalba\tuza\composer\Join as JoinComposer;

use xibalba\tuza\composer\dialects\Limit as LimitComposer;
use xibalba\tuza\composer\dialects\Top as TopComposer;

use xibalba\tuza\statement\Select as SelectStatement;
use xibalba\tuza\syntax\Target as TargetSyntax;

use \xibalba\tuza\clause\Where as WhereClause;

use \Ds\Vector;

/**
 * This class provide an OOP API for query building.
 *
 * @package xibalba\tuza\syntax
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Select {

	public static function compose(SelectStatement $statement) : string {
		$parts = new Vector();

		if(!$statement->isDistinct()) $parts->push("SELECT");
		else $parts->push("SELECT DISTINCT");

		// if there is a TOP dialect clause, then goes before columns expression
		if($statement->hasTop()) $parts->push(TopComposer::compose($statement->getTop()));

		$parts->push(static::composeColumns($statement));
		$parts->push(static::composeFrom($statement->getTarget()));

		// Not all queries has joins
		if($statement->hasJoin()) $parts->push(static::composeJoin($statement->getAllJoins()));

		// Not all queries has wheres
		if($statement->hasWhere()) $parts->push(WhereComposer::composeClause($statement->getWhere()));

		// Not all queries has order by
		if($statement->hasOrderBy()) $parts->push(static::composeOrderBy($statement->getOrderBy()));

		// compose dialects clauses
		if($statement->hasLimit()) $parts->push(LimitComposer::compose($statement->getLimit()));

		return trim($parts->join(" "));
	}

	public static function composeColumns(SelectStatement $statement) : string {
		$cols = new Vector();
		$allCols = $statement->getAllColumns();

		foreach ($allCols as $coll) {
			$cols->push(ColumnsComposer::composeWithAlias($coll));
		}

		$fnCols = $statement->getColumnsExpression()->getColumnsFunction();
		if($fnCols->count() > 0) $cols->push(ColumnsComposer::composeFunctionsAsColumns($fnCols));

		return $cols->join(", ");
	}

	public static function composeFrom(TargetSyntax $target) : string {
		return "FROM " . FromComposer::composeWithAlias($target);
	}

	public static function composeOrderBy(Vector $allOrderBy) : string {
		$parts = new Vector();

		foreach ($allOrderBy as $orderBy) {
			$parts->push(ColumnsComposer::compose($orderBy->getColumns()) . " " . $orderBy->getDirection());
		}

		return "ORDER BY " . $parts->join(", ");
	}

	public static function composeJoin(Vector $joins) : string {
		$parts = new Vector();
		foreach($joins as $j) $parts->push(JoinComposer::compose($j));
		return $parts->join(" ");
	}
}
