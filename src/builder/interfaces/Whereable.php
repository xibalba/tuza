<?php
/**
 * @copyright	2014 - 2024 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\builder\interfaces;

use xibalba\tuza\clause\OrderBy as OrderByClause;
use Ds\Vector;

/**
 * This interface define the basic signature for a Wereable class.
 * A Whareable class are those that abstract a WHERE clause as part
 * of it abstracted sentence.
 */
interface Whereable {
	/**
	 * Allow to compose a WHERE clause.
	 *
	 * @return Whereable
	 */
	public function where() : Whereable;

	/**
	 * Add a sub where to an actual WHERE clause.
	 *
	 * @param string $conjunction
	 * @return Whereable
	 */
	public function subWhere(string $conjunction) : Whereable;

	/**
	 * Add an equals (=) comparison.
	 *
	 * @param string $column to compare
	 * @param $value to compare
	 * @return Whereable
	 */
	public function equals(string $column, $value) : Whereable;

	/**
	 * Add a between (BETWEEN) comparison.
	 *
	 * @param mixed $value Subject to compare. Provide a string or number as value, or a `syntax\Column` instance for a target column.
	 * @param mixed $min Minimum value or column of the range for compare to.
	 * @param mixed $max Maximum value or column of the range for compare to.
	 * @return Whereable
	 */
	function between(mixed $value, mixed $min, mixed $max) : Whereable;

	/**
	 * Add a not between (NOT BETWEEN) comparison.
	 *
	 * @param mixed $value Subject to compare. Provide a string or number as value, or a `syntax\Column` instance for a target column.
	 * @param mixed $min Minimum value or column of the range for compare to.
	 * @param mixed $max Maximum value or column of the range for compare to.
	 * @return Whereable
	 */
	public function notBetween(mixed $value, mixed $min, mixed $max) : Whereable;

	/**
	 * Add a between (BETWEEN) comparison for a target column against a range of values.
	 *
	 * @param string $column Target column name to compare.
	 * @param mixed $min Minimum range value to compare.
	 * @param mixed $max Maximum range value to compare.
	 * @return Whereable
	 */
	public function columnBetweenValues(string $column, mixed $min, mixed $max) : Whereable;

	/**
	 * Add a not between (NOT BETWEEN) comparison for a target column against a range of values.
	 *
	 * @param string $column Target column name to compare.
	 * @param mixed $min Minimum range value to compare.
	 * @param mixed $max Maximum range value to compare.
	 * @return Whereable
	 */
	public function columnNotBetweenValues(string $column, mixed $min, mixed $max) : Whereable;

	/**
	 * Add a between (BETWEEN) comparison for a value against a range defined by target columns.
	 *
	 * @param mixed $value Value to comparte.
	 * @param string $columnMin Target column name for minimum rage to comparte to.
	 * @param string $columnMax Target column name for maximum rage to comparte to.
	 * @return Whereable
	 */
	public function valueBetweenConlumns(mixed $value, string $columnMin, string $columnMax) : Whereable;

	/**
	 * Add a not between (NOT BETWEEN) comparison for a value against a range defined by target columns.
	 *
	 * @param mixed $value Value to comparte.
	 * @param string $columnMin Target column name for minimum rage to comparte to.
	 * @param string $columnMax Target column name for maximum rage to comparte to.
	 * @return Whereable
	 */
	public function valueNotBetweenConlumns(mixed $value, string $columnMin, string $columnMax) : Whereable;

	/**
	 * Add a non equals (!=) comparison.
	 *
	 * @param string $column to compare.
		 * @param $value to compate.
	 * @return Whereable
	 */
	public function notEquals(string $column, $value) : Whereable;

	/**
	 * Add a greater than (>) comparison.
	 * @param string $column to compare.
	 * @param $value to compare.
	 * @return Whereable
	 */
	public function greaterThan(string $column, $value) : Whereable;

	/**
	 * Add a greater than or equal (>=) comparison.
	 * @param string $column to compare.
	 * @param $value to compare.
	 * @return Whereable
	 */
	public function greaterThanOrEqual(string $column, $value) : Whereable;

	/**
	 * Add a less than (<) comparison.
	 * @param string $column to compare.
	 * @param $value to compare.
	 * @return Whereable
	 */
	public function lessThan(string $column, $value) : Whereable;

	/**
	 * Add a less than or equal (<=) comparison.
	 * @param string $column to compare.
	 * @param $value to compare.
	 * @return Whereable
	 */
	public function lessThanOrEqual(string $column, $value) : Whereable;

	/**
	 * Add a Like (LIKE) comparison.
	 * @param string $column to compare.
	 * @param $value to compare.
	 * @return Whereable
	 */
	public function like(string $column, $value) : Whereable;

	/**
	 * Add a not like (NOT LIKE) comparison.
	 * @param string $column to compare.
	 * @param $value to compare.
	 * @return Whereable
	 */
	public function notLike(string $column, $value) : Whereable;

	/**
	 * Add an in (IN) comparison.
	 * @param string $column to compare,
	 * @param Vector $values set of values to compare.
	 * @return Whereable
	 */
	public function in(string $column, Vector $values) : Whereable;

	/**
	 * Add a not in (NOT IN) comparison.
	 * @param string $column to compare,
	 * @param Vector $values set of values to compare.
	 * @return Whereable
	 */
	public function notIn(string $column, Vector $values) : Whereable;

	/**
	 * Add an order by (ORDER BY) expression.
	 * @param $columns for order to.
	 * @param string $direction order direction.
	 * @param string|null $target Optional target if there is a JOIN target to.
	 * @return Whereable
	 */
	public function orderBy($columns, string $direction = OrderByClause::ASC, ?string $target = null) : Whereable;

	/**
	 * Close a subwhere call reference.
	 * @return Whereable
	 */
	public function endWhere() : Whereable;
}