<?php
/**
 * @copyright	2014 - 2024 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\builder;

use xibalba\tuza\builder\interfaces\Whereable;
use xibalba\tuza\builder\traits\WhereAdapter;

use xibalba\tuza\statement\Update as UpdateStatement;

use xibalba\tuza\syntax\Factory as FactorySyntax;

use xibalba\tuza\composer\Update as UpdateComposer;

use \Ds\Map;

class Update implements Whereable {
	use WhereAdapter;

	private $__updateStatement;

	private function _getStatement() : UpdateStatement {
		return $this->__updateStatement;
	}

	/**
	 * Update constructor
	 * @param string|null $targetName Optional target name
	 * @param string $schema Optional schema name
	 */
	public function __construct(?string $targetName = null, string $schema = "") {
		$this->__updateStatement = new UpdateStatement();
		if(!is_null($targetName)) $this->setTarget($targetName, $schema);
	}

	/**
	 * Set builder's target, a table or views name. Optionally a schema name can be setted.
	 *
	 * @param string $name Target name
	 * @param string $schema Optional schema name
	 * @return $this
	 */
	public function setTarget(string $name, string $schema = "") : Update {
		if($this->_getStatement()->hasTarget()) throw new \RuntimeException("You can set the builder's Target once.");
		$this->_getStatement()->setTarget(FactorySyntax::createTarget(name: $name, schema: $schema));

		return $this;
	}

	/**
	 * Set a map of colmuns-values pairs to be setted for UPDATE at the composed sentense.
	 * @param array|Map $values map fo column-values pairs to be setted.
	 * @return $this
	 */
	public function setValues(array|Map $values) : Update {
		if(is_array($values)) {
			if(count($values) > 0 && array_keys($values) !== range(0, count($values) - 1)) $this->setMap(new Map($values));
			else throw new \InvalidArgumentException("You must provide a key-value pairs array or a Map instace with at least one pair.");
		}
		else $this->setMap($values);

		return $this;
	}

	/**
	 * Set a map of colmuns-values pairs to be setted for UPDATE at the composed sentense.
	 * This method has the same effect that `setValues`, but does not suppor array parameter.
	 * @see setValues()
	 * @param Map $map map fo column-values pairs to be setted.
	 * @return $this
	 */
	public function setMap(Map $map) : Update {
		if($map->count() > 0) $this->_getStatement()->setPairs($map);
		else throw new \InvalidArgumentException("You must provide a key-value pairs Map instace with at least one pair.");
		return $this;
	}

	/**
	 * Bind a provided value to the given column name.
	 * @param string $columnName target column name to set value.
	 * @param $value the value to be setted at the provided column name.
	 * @return $this
	 */
	public function set(string $columnName, $value) : Update {
		$this->_getStatement()->set($columnName, $value);
		return $this;
	}

	/**
	 * Retrive the composed UPDATE statement string.
	 * @return string
	 */
	public function getSql() : string {
		return UpdateComposer::compose($this->_getStatement());
	}
}