<?php

namespace guatuza\composer;

use PHPUnit\Framework\TestCase;

use xibalba\tuza\syntax\Column as ColumnSyntax;
use xibalba\tuza\syntax\Target as TargetSyntax;

use xibalba\tuza\composer\Columns as ColumnsComposer;

use Ds\Map;
use Ds\Vector;

class ColumnsTest extends TestCase {
	public function testCompose() {
		$expected = "generic.foo";
		$expectedAliased = "generic.foo AS alias";

		$table = new TargetSyntax("generic");
		$column = new ColumnSyntax("foo", $table);
		$vColumns = new Vector([$column]);

		$this->assertSame($expected, ColumnsComposer::compose($vColumns));

		$column->setAlias("alias");
		$this->assertSame($expectedAliased, ColumnsComposer::composeWithAlias($column));
	}

	public function testFunctions() {
		$expected = "MAX(generic.foo), MIN(generic.baz) AS min";

		$foo = new ColumnSyntax("foo", new TargetSyntax("generic"));
		$baz = new ColumnSyntax("baz", new TargetSyntax("generic"));

		$fnMax = new Map();
		$fnMin = new Map();

		$fnMax->put("function", "MAX");
		$fnMax->put("args", new Vector([$foo]));

		$fnMin->put("function", "MIN");
		$fnMin->put("args", new Vector([$baz]));
		$fnMin->put("alias", "min");

		$fns = new Vector([$fnMax, $fnMin]);

		$this->assertSame($expected, ColumnsComposer::composeFunctionsAsColumns($fns));
	}
}