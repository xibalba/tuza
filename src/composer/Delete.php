<?php
/**
 * @copyright	2014 - 2021 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\composer;

use xibalba\tuza\composer\Where as WhereComposer;
use xibalba\tuza\statement\Delete as DeleteStatement;

class Delete {
	public static function compose(DeleteStatement $statement) : string {
		$target = $statement->getTarget()->getQualifiedName();

		// Not all queries has wheres
		$where = "";
		if($statement->hasWhere()) $where = WhereComposer::composeClause($statement->getWhere());

		return  "DELETE FROM $target $where";
	}
}