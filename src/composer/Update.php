<?php
/**
 * @copyright	2014 - 2020 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\composer;

use xibalba\tuza\composer\Where as WhereComposer;
use xibalba\tuza\statement\Update as UpdateStatement;

use \Ds\Vector;
use \Ds\Map;

/**
 * This class provide an OOP API for query building an UPDATE sentence.
 *
 * @package xibalba\tuza\syntax
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Update {
	public static function compose(UpdateStatement $statement) : string {
		$table = $statement->getTarget()->getQualifiedName();

		// Not all queries has wheres
		$where = "";
		if($statement->hasWhere()) $where = " " . WhereComposer::composeClause($statement->getWhere());

		return  "UPDATE $table SET " . static::composePairs($statement->getPairs()) . "$where";
	}

	public static function composePairs(Map $pairs) : string {
		$cols = new Vector();

		foreach ($pairs as $key => $value) {
			$cols->push($key->getName() . " = " . Placeholder::composeValidSqlValue($value));
		}

		return $cols->join(", ");
	}
}