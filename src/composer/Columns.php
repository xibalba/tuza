<?php
/**
 * @copyright    2014 - 2020 Xibalba Lab.
 * @license    http://opensource.org/licenses/MIT
 * @link        https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\composer;

use xibalba\tuza\syntax\Column as ColumnSyntax;

use Ds\Vector;

class Columns {
	/**
	 * Compose a column name with AS clause for alias when apply.
	 * @param ColumnSyntax $column
	 * @return string
	 */
	public static function composeWithAlias(ColumnSyntax $column) : string {
		if($column->hasAlias() && !$column->isAll()) return static::composeSingle($column) . " AS " . $column->getAlias();
		else return static::composeSingle($column);
	}

	public static function compose(Vector $columns) : string {
		$parts = new Vector();
		foreach ($columns as $col) $parts->push(static::composeSingle($col));
		return $parts->join(", ");
	}

	public static function composeList(Vector $columns) : string {
		$parts = new Vector();
		foreach ($columns as $col) $parts->push($col->getName());
		return $parts->join(", ");
	}

	/**
	 * Compose a string with a column name attached with its table name
	 * @param ColumnSyntax $column
	 * @return string
	 */
	public static function composeSingle(ColumnSyntax $column) : string {
		// If the table has an Alias then the column name must be `tableAlias.columnName`
		// Otherwise must use the qualified name of the table like `schema.tableName.columnName`
		return ($column->getTarget()->hasAlias()
			? $column->getTarget()->getAlias()
			: $column->getTarget()->getQualifiedName()) . "." . $column->getName();
	}

	protected static function _composeFunctionArguments(Vector $arguments) : string {
		$composedArgs = new Vector();

		foreach ($arguments as $arg) {
			if($arg instanceof ColumnSyntax) $composedArgs->push(static::composeSingle($arg));
			else $composedArgs->push(Placeholder::composeValidSqlValue($arg));
		}

		return $composedArgs->join(", ");
	}

	public static function composeFunctionsAsColumns(Vector $functionColumns) : string {
		$composedParts = new Vector();
		$tmpPart = null;

		foreach ($functionColumns as $fnc) {
			$tmpPart = mb_strtoupper($fnc->get("function"));
			$tmpPart .= $fnc->get("args")->count() > 0
				? "(" . static::_composeFunctionArguments($fnc->get("args")) . ")"
				: "()";

			if($fnc->hasKey("alias") && mb_strlen($fnc->get("alias")) > 0) $tmpPart .= " AS " . $fnc->get("alias");

			$composedParts->push($tmpPart);
		}

		return $composedParts->join(", ");
	}
}
