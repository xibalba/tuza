<?php

namespace guatuza;

use PHPUnit\Framework\TestCase;

use xibalba\tuza\syntax\Target as TargetSyntax;
use xibalba\tuza\composer\From as FromComposer;

class TargetTest extends TestCase {
	public function testQualifiedName() {
		$target = new TargetSyntax("table", "alias", "schema");
		$expected = "schema.table";
		$expectedWithAlias = "schema.table AS alias";

		$this->assertSame($expected, $target->getQualifiedName());
		$this->assertSame("alias", $target->getAlias());
		
		$this->assertSame($expected, FromComposer::composeWithSchema($target));
		$this->assertSame($expectedWithAlias, FromComposer::composeWithAlias($target));
	}
}