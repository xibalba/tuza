# Tuza: compositor de consultas y biblioteca ligera de base de datos

Tuza es un compositor ligero de consultas SQL y un envolvente de conexión a bases de datos sobre PDO. El principal objetivo de Tuza es proveer de una elegante API orientada a objetos para la composición de consultas SQL complejas con PHP.

La composición de consultas no requiere de ninguna conexión con ningún motor de base de datos, sin embargo Tuza también provee unas clases livianas sobre PDO para facilitar la tarea de conexión.

## 1. Requerimientos

Tuza requiere PHP8 o posterior y la extensión *PHP Data Structures* para funcionar eficientemente, no obstante para desarrollo y pruebas se puede prescindir de la instalación de dicha extensión y Tuza funcionará transparentemente con el *pollyfill* de *PHP Data Structures* que será instalado automáticamente por *Composer*.

## 2. Instalación

La forma recomendada de instalar Tuza en mediante composer:

```sh
php composer.phar require xibalba/tuza
```

## 3. Composición de consultas

El DML del SQL estándar consiste de cuatro sentencias verbales que permiten la selección (`SELECT`), inserción (`INSERT`), actualización (`UPDATE`) y eliminación (`DELETE`) de datos. Tuza provee una clase compositora homónima (en *Camelcase*) para cada sentencia. Se recomienda, por legibilidad, importar la clase o clases de interés y asignarle(s) un alias:

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\builder\Insert as InsertBuilder;
use xibalba\tuza\builder\Update as UpdateBuilder;
use xibalba\tuza\builder\Delete as DeleteBuilder;
```

El objetivo de emplear Tuza es poder construir las consultas de forma dinámica y obtener al final una cadena con la consulta SQL bien formada que pueda ejecutarse (o imprimirse) transparentemente.

Por ejemplo, para obtener todos los regristros de todas las columnas de una tabla `table_name` en SQL se escribe:

```sql
SELECT table_name.* FROM table_name
```

Con Tuza se puede componer dicha consulta de la siguiente manera:

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\syntax\Column as ColumnSyntax;

$builder = new SelectBuilder();
$builder->from("table_name")
    ->setColumns(ColumnSyntax::ALL);

$sql = $builder->getSql();
echo $sql;
```

La clase `Select` admite opcionalmente en el contructor tres parámetros: la cadena con el nombre de la *fuente* de datos, que corresponde al nombre de una tabla o vista; un alias para dicha *fuente* y la cadena con el nombre del esquema de la *fuente*. Estos mismos parámetros tambíen pueden establecerse al llamar el método `from()`.

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\syntax\Column as ColumnSyntax;

// Definición de parámetros en el constructor.
$builderA = new SelectBuilder("the_target", "as_alias", "at_schema");
    ->setColumns(ColumnSyntax::ALL);

// Definición de parámetros en el método `from()`.
$builderB = new SelectBuilder();
$builderB->from("the_target", "as_alias", "at_schema")
    ->setColumns(ColumnSyntax::ALL);

echo $builderA->getSql();
echo $builderB->getSql();
```

Tanto `$builderA` como `$builderB` darán exactamente el mismo resultado:

```sql
SELECT as_alias.* FROM at_schema.the_target AS as_alias
```

La misma dinámica se aplica para los otros compositores, por ejemplo, para actualizar unos datos determinados se puede elaborar una consulta como la siguiente:

```sql
UPDATE fake SET str_col = 'text', numeric_col = 6, date_col = :date_val, null_col = NULL
WHERE (fake.bar = :woo) AND (fake.baz > 6)
```

En este ejemplo `date_col` y `fake.bar` se les ha asociado un *placeholder* que se sustituye por un valor concreto al momento de ejecutar la sentencia mediante PDO.

Para componer esta consulta con Tuza se empleará la clase `Update`:

```php
use xibalba\tuza\builder\Update as UpdateBuilder;

// Crear una instancia proveyendo en el constructar el *target* (tabla a vista objetivo).
$uBuilder = new UpdateBuilder("fake");

// Establecer los nombres de columna y sus valores estáticos o *placeholders*.
$uBuilder->set("str_col", "text")
    ->set("numeric_col", 6)
    ->set("date_col", ":date_val")
    ->set("null_col", null);

// Establecer las condiciones de la consulta
$uBuilder->where()
    ->equals("bar", ":woo")
    ->greaterThan("baz", 6);

// Obtener el SQL resultante
echo $uBuilder->getSql();
```

Como puede observarse, los métodos de las clases compositoras devuelven la propia instancia, por lo que es posible concatenar llamadas a los distintos métodos disponibles en cada clase.

## 4. Recetario breve

A continuación se provee de un breve *recetario* de ejemplos para las cuatro clases compositoras.

### 4.1 Componiendo sentencias `SELECT`

#### 4.1.1 Componiendo sentencias `SELECT` básicas

Las sentencias `SELECT` básicas se componen indicando una lista de columnas de datos a recuperar, la *fuente* de dichos datos (una tabla o una vista) y unas condiciones para dicha selección. Como ya se observó más arriba, la tabla o vista de trabajo, junto con su alias y esquema (opcionales) se pueden establecer tanto en el constructor de la clase como llamando el método `from()`. Por otra parte, la lista de columnas de datos a recuperar se establece mediante el método `setColumns()`. La lista de parámetros que se puede establecer con `setColumns()` puede ser tan larga como haga falta.

**Ejemplo 1:** Sentencia `SELECT` básica

```php
use xibalba\tuza\builder\Select as SelectBuilder;

$builder = new SelectBuilder();
$builder->from("table_name")
    ->setColumns("column_a", "column_b", "column_c");

echo $builder->getSql();
```

**Resultado:**

```sql
SELECT table_name.column_a, table_name.column_b, table_name.column_c FROM table_name
```

Para indicar que se desea obtener todas las columnas disponibles en la *fuente* se emplea el asteristo `*`, en Tuza se puede indicar mediante el literal o bien, mediante la constante definida en `syntax\Column::ALL`, disponible principalmente por razones de legibilidad.

**Ejemplo 2:** Sentencia `SELECT` básica con todas las columnas

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\syntax\Column as ColumnSyntax;

$builderA = new SelectBuilder("table_name");
$builderB = new SelectBuilder("table_name");

$builderA->setColumns("*");
$builderB->setColumns(ColumnSyntax::ALL);

echo $builderA->getSql();
echo $builderB->getSql();
```

**Resultado:**

```sql
SELECT table_name.* FROM table_name
SELECT table_name.* FROM table_name
```

Tanto `$builderA` como `$builderB` componen el mismo resultado.

#### 4.1.2 Componiendo sentencias `SELECT` con alias

Muchas veces se necesita establecer alias no solo a las tablas o vistas de consulta, sino también a las columnas. Cuando este es caso, se debe emplear el método `putColumns()` en lugar de `setColumns()`. El método `putColumns()` espera un solo parámetro de tipo `array` (de clave-valor) o `Ds\Map` donde la clave es el nombre de la columna y el valor el nombre del alias.

**Ejemplo 3:** Sentencia `SELECT` básica con alias

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use Ds\Map;

$cMap = new Map();
$cMap->put("col_a", "AL_W");
$cMap->put("col_b", "AL_Y");
$cMap->put("col_c", "AL_Z");

$builderA = new SelectBuilder("table_name", "talias");
$builderB = new SelectBuilder("table_name", "talias");

$builderA->putColumns(["col_a" => "AL_W", "col_b" => "AL_Y", "col_c", => "AL_Z"]);
$builderB->putColumns($cMap);

echo $builderA->getSql();
echo $builderB->getSql();
```

**Resultado:**

```sql
SELECT talias.col_a AS AL_W, talias.col_b AS AL_Y, talias.col_c AS AL_Z FROM table_name AS talias
```

Tanto `$builderA` como `$builderB` componen el mismo resultado.

#### 4.1.3 Componiendo sentencias `Select` con funciones en columnas

Hay casos en los que en lugar de una columna se necesita consultar el resultado de una función. La forma más común son aquellas funciones que operan sobre una columna concreta, por ejemplo `MAX()` o `MIN()` para obtener el valor máximo o minimo de los datos en funcion de la columna proveída. Para estos casos Tuza provee el método `addFunctionOnColumn()` que espera tres parámetros: (1) el nombre de la función, (2) la columna a operar y (3) el alias de la columna. En tanto que los parámetros no son más que cadenas, se puede suministrar cualquier nombre de funcion que soporte el motor de base de datos objetivo, la única restricción es que dicha función solo soparta un nombre de columna como parámetro.

**Ejemplo 4:** Sentencia `SELECT` básica con funciones

```php
use xibalba\tuza\builder\Select as SelectBuilder;

$sBuilder = new SelectBuilder("table_name");
$sBuilder->addFunctionOnColumn("MAX", "u_id", "total_id");

echo $sBuilder->getSql();
```

**Resultado:**

```sql
SELECT MAX(table_name.u_id) AS total_id FROM table_name
```

#### 4.1.4 Sentencias `Select` con `TOP`, `COUNT` y `LIMIT`

Dependiendo del motor de base de datos, se puede limitar la cantidad de resultados devueltos mediante `TOP` o `LIMIT`, además de poder obtener el conteo de resultados mediante `COUNT`. Para estas tres funciones Tuza provee métodos análogos.

**Ejemplo 5:** Sentencia `SELECT` con `TOP`, `COUNT` y `LIMIT`

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\syntax\Column as ColumnSyntax;
use Ds\Map;

$sBuilderCount = new SelectBuilder("table_name");
$sBuilderCount->count("col_name", "calias");

$sBuilderTop = new SelectBuilder("table_name");
$sBuilderTop->setColumns(ColumnSyntax::ALL)->top(9);

$sBuilderLimit = new SelectBuilder("table_name");
$sBuilderLimit->setColumns(ColumnSyntax::ALL)->limit(9);

echo $sBuilder->getSql();
```

**Resultado:**

```sql
SELECT COUNT(table_name.col_name) AS calias FROM table_name
SELECT TOP 9 table_name.* FROM table_name
SELECT table_name.* FROM table_name LIMIT 9
```

#### 4.1.5 Componiendo sentencias `Select` con condiciones

Lo usual al definir consultas con `SELECT` es que vayan acompañadas de una cláusula `WHERE` que define las condiciones de selección de los datos. La clase `Select` de Tuza provee un método `where()` al que se le pueden ir concatenando métodos de comparación para establecer las condiciones que se necesitan en la consulta.

**Ejemplo 6:** Sentencia `SELECT` básica con condiciones

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use Ds\Map;

$sBuilder = new SelectBuilder();
$sBuilder->from("table_name", "talias")
    ->setColumns("col_a", "col_b", "col_c")
    ->where()
        ->equals("col_x", "foo_bar");

echo $sBuilder->getSql();
```

**Resultado:**

```sql
SELECT talias.col_a, talias.col_b, talias.col_c FROM table_name AS talias WHERE (talias.col_x = 'foo_bar')
```

Tuza abstrae todos los operadores de comparación de SQL en métodos análogos con el orden de parámetros `{x}("col_name", value)` donde `col_name` es el nombre de la columna a coparar y `value` es el valor de comparación (excepto para los operados IS NULL y IS NOT NULL que no requieren de un parámetro de valor).

A continuación se provee una tabla con todos los operadores soportados:

| Operador SQL  |Método                 |Ejemplo                            |SQL                            |
|---------------|-----------------------|-----------------------------------|-------------------------------|
| `=`           |`equals()`             |`->equals("col_x", "str")`         |`target.col_x = 'str'`         |
| `<>`          |`notEquals()`          |`->notEquals("col_x", 66)`         |`target.col_x <> 66`           |
| `>`           |`greaterThan()`        |`->greaterThan("col_x", 99)`       |`target.col_x > 99`            |
| `>=`          |`greaterThanOrEqual()` |`->greaterThanOrEqual("col_x", 99)`|`target.col_x >= 99`           |
| `<`           |`lessThan()`           |`->lessThan("col_x", 22)`          |`target.col_x < 22`            |
| `<=`          |`lessThanOrEqual()`    |`->lessThanOrEqual("col_x", 22)`   |`target.col_x <= 99`           |
| `LIKE`        |`like()`               |`->like("col_x", "simil")`         |`target.col_x LIKE 'simil'`    |
| `NOT LIKE`    |`notLike()`            |`->notLike("col_x", "simil")`      |`target.col_x NOT LIKE 'simil'`|
| `IS NULL`     |`isNull()`             |`->isNull("col_x")`                |`target.col_x IS NULL`         |
| `IS NOT NULL` |`isNotNull()`          |`->isNotNull("col_x")`             |`target.col_x IS NOT NULL`     |

El operador `BETWEEN` y `NOT BETWEEN` es un poco diferente debido a que se debe proveer el valor mínimo y máximo del rango de comparación del objeto de interés. Tuza provee seis métodos para la composición de comparaciones `BETWEEN`.

##### 4.1.5.1 Comparar columna entre valores

Tuza provee dos métodos para comparar columnas entre valores:

1. `columnBetweenValues(string $column, mixed $min, mixed $max)`
2. `columnNotBetweenValues(string $column, mixed $min, mixed $max)`

Donde `$column` es una cadena con el nombre de una columna válida para el `target` establecido en el `SelectBuilder`, mientras que `$min` y `$max` son los valores mínimo y máximo del rango a comparar.

**Ejemplo 6.1:** Sentencia `SELECT` con condición `BETWEEN`

```php
use xibalba\tuza\builder\Select as SelectBuilder;

$sBuilder = new SelectBuilder();
$sBuilder->from("table_name")
    ->setColumns("*")
    ->where()
        ->columnBetweenValues("col_x", 66, 99);
```

**Resultado:**

```sql
SELECT table_name.* FROM table_name WHERE (table_name.col_x BETWEEN 66 AND 99)
```

##### 4.1.5.2 Comparar un valor entre columnas

Tuza provee dos métodos para comparar valores entre columnas:

1. `valueBetweenColumns(mixed $value, string $columnMin, string $columnMax)`
2. `valueNotBetweenColumns(mixed $value, string $columnMin, string $columnMax)`

Donde `$value` es un valor numérico a una cadena de fecha, mientras que `$min` y `$max` son los nombres válidos de columna para el `$target` establecido en el `SelectBuilder`.

**Ejemplo 6.2:** Sentencia `SELECT` con condición `BETWEEN`

```php
use xibalba\tuza\builder\Select as SelectBuilder;

$sBuilder = new SelectBuilder();
$sBuilder->from("table_name")
    ->setColumns("*")
    ->where()
        ->valueBetweenColumns(66, "col_x", "col_y");
```

**Resultado:**

```sql
SELECT table_name.* FROM table_name WHERE (66 BETWEEN table_name.col_x AND table_name.col_y)
```

##### 4.1.5.3 Comparar un valor o columna entre valores o columnas

Además de los métodos recién señalados, tuza provee otros dos métodos que permiten establecer una columna o valor para cada argumento:

1. `between(mixed $value, mixed $min, mixed $max)`
2. `notBetween(mixed $value, mixed $min, mixed $max)`

Para pasar un valor, basta proveer una cadena o número en cualquiera de los argumentos. Para pasar una columna se debe proveer una instancia de `syntax\Column`, la cual puede crearse fácilmente con el método fábrica `createColumn()` de la clase `syntax\Factory`.

**Ejemplo 6.3:** Sentencia `SELECT` con condición `BETWEEN`

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\syntax\Factory as SyntaxFactory;

$sBuilder = new SelectBuilder();
$sBuilder->from("table_name")
    ->setColumns("*")
    ->where()
        ->between(
            SyntaxFactory::createColumn("foo", null, $target),
            66,
            SyntaxFactory::createColumn( "baz", null, $target),
        )
```

**Resultado:**

```sql
SELECT table_name.* FROM table_name WHERE (table_name.foo BETWEEN 66 AND table_name.baz)
```

#### 4.1.6 Componiendo sentencias `Select` ordenadas

Para ordenar ascendente o descendentemente los resultados de una consulta, se emplea la cláusula `ORDER BY`, la cual se encuentra abstraida en Tuza en el método `orderBy()`. Este método espera como primer parámetro una cadena con el nombre de una columna, o bien, un arreglo o instancia de `Vector` con la lista de nombres de columnas para ordenar. Por defecto compondrá el orden como ascendente, pero se puede indicar como segundo parámetro mediante la constante `clause\OrderBy::DESC` para que se componga la cadena indicando un ordenamiento descendente.

**Ejemplo 7:** Sentencia `SELECT` con cláusula `ORDER BY`

```php
use xibalba\tuza\builder\Select as SelectBuilder;
use xibalba\tuza\clause\OrderBy;

$builderSimple = new SelectBuilder("generic");
$builderSimple->setColumns("*")->orderBy("foo");

$builderMultiple = new SelectBuilder("generic");
$builderMultiple->setColumns("*")
    ->orderBy(["foo", "zaz"])
    ->orderBy("baz", OrderBy::DESC);

echo $builderSimple->getSql();
echo $builderMultiple->getSql();
```

**Resultado:**

```sql
SELECT generic.* FROM generic ORDER BY generic.foo ASC
SELECT generic.* FROM generic ORDER BY generic.foo, generic.zaz ASC, generic.baz DESC
```

#### 4.1.7 Componiendo sentencias `SELECT` con `JOIN`

La clase `Select` soporta cuatro formas de cláusulas `JOIN`, cada una con su método correspondiente. Para los cuatro casos se ha de proveer un parámetro con el nombre de la tabla o vista (aka *fuente*) y una lista de columnas seleccionables de dicha *fuente*. El parámetro de columnas puede ser un `array` indexado o `Ds\Vector` de cadenas, o bien un `array` de claves-valor o instancia de `Ds\Map` donde cada clave es un nombre de columna y cada valor un alias para dicha columna. Si solo se desea establecer alias para algunas columnas y para otras no, entonces para aquellas que no se desea asignar alias se debe asignar `null` como valor de la clave (columna) de interés (el ejemplo lo dejará más claro).

A continuación se provee una tabla de ejemplo con los cuatro métodos `join` soportados:

|SQL JOIN    |Método        |Ejemplo                                                          |
|------------|--------------|-----------------------------------------------------------------|
|`INNER JOIN`|`innerJoin()` |`->innerJoin("target_x", ["col_a", "col_b"])`                    |
|`LEFT JOIN` |`leftJoin()`  |`->leftJoin("target_x", ["col_a" => "c_alias", "col_b" => null])`|
|`RIGHT JOIN`|`rightJoin()` |`->rightJoin("target_x", $vectorInstance)`                       |
|`CROSS JOIN`|`crossJoin()` |`->crossJoin("target_x", $mapInstance)`                          |

Donde `$vectorInstance` y `$mapInstance` pueden definirse así:

```php
use Ds\Vector;
use Ds\Map;

$vectorInstance = new Vector();
$vectorInstance->push("col_a");
$vectorInstance->push("col_b");

$mapInstance = new Map();
$mapInstance->put("col_a", "c_alias");
$mapInstance->put("col_b", null);
```

Como puede observarse, las columnas a consultar de las fuentes unidas por `JOIN` se establecen en el método correspondiente y no llamando a los métodos `setColumns()` ni `putColumns()` de la clase `Select`, las columnas establecidas por estos métodos serán asociadas exclusivamente a la *fuente* definida en el constructor o en el método `from()`. Este comportamiento permite una composición dinámica muy flexible, posibilitando la construcción de consultas a la medida en función de las condiciones que el(la) desarrollador(a) requiera.

La cláusula `JOIN` debe ir acompañada de unas condiciones, las que se establecen exactamente igual que con la cláusula `WHERE`. Tras llamar a uno de los métodos `{x}Join()` se debe llamar al método `on()` para indicar al compositor que las llamadas a los métodos de comparación corresponden al `JOIN` y no al `WHERE`.

**Ejemplo 8:** Sentencia `SELECT` con cláusula `JOIN`

```php
use xibalba\tuza\builder\Select as SelectBuilder;

$builder = new SelectBuilder();
$builder->from("user")
    ->putColumns([
        "user_id" => "userId",
        "name" => "username",
        "email" => null,
        "created_at" => null
    ])
    ->leftJoin("news", ["title" => "newsTitle", "body" => null, "updated_at" => null])
        ->on()
        ->equals('author_id', $builder->getColumn('user_id'))
        ->equals('author_id', ":authorId");

$builder->innerJoin("editors")
    ->on()
    ->equals("author_id", $builder->getColumn('user_id'))
    ->equals("author_id", ":authorId")
->where()
    ->lessThan("user_id", ":userId")
    ->notLike("name", ":userNames")
->orderBy("user_id", OrderBy::DESC)
->orderBy("updated_at", OrderBy::DESC, "news");
```

**Resultado:**

```sql
SELECT user.user_id AS userId, user.name AS username, user.email, user.created_at, news.title AS newsTitle, news.body, news.updated_at FROM user LEFT JOIN news ON (news.author_id = user.user_id) AND (news.author_id = :authorId) INNER JOIN editors ON (editors.author_id = user.user_id) AND (editors.author_id = :authorId) WHERE (user.user_id < :userId) AND (user.name NOT LIKE :userNames) ORDER BY user.user_id DESC, news.updated_at DESC
```

### 4.2 Componiendo sentencias `INSERT`

La sentencia `INSERT` está abstraida en Tuza en la clase `builder\Insert`. Esta es la sentencia y clase más sencilla de operar. La tabla objetivo se debe indicar siempre en el constructor, opcionalmente puede proveerse un segundo parámetro con la cadena del esquema de dicha tabla. La clase `Insert` provee de distintos métodos que flexibilizan la forma de establecer las columnas y datos a insertar.

#### 4.2.1 Componiendo sentencias `INSERT` mediate agregaciones simples

Para preparar una sentencia `INSERT` mediante agregaciones simples se debe establecer primero la lista de columnas a afectar mediante el método `setColumns()`, el cual espera una lista armitraria de cadenas. El orden de dichos parámetros es importante ya que determina el orden en que se compondrá el SQL resultante. Posteriarmente se pueden agregar valores a la instancia del compositor mediante el método `addValues()`. Este método espera una lista de valores arbitrarios. El usuario es responsable de garantizar que los datos agregados mediante `addValues()` correspondan en orden y tipo con las columnas establecidas mediante `setColumns()`.

**Ejemplo 1:** Sentencia `INSERT` con agregaciones simples

```php
use xibalba\tuza\builder\Insert as InsertBuilder;

$iBuilder = new InsertBuilder("fake");
$iBuilder->setColumns("str_col", "numeric_col", "date_col", "null_col");
$iBuilder->addValues("str_val", 6.66, '2020-06-01', null);

echo $iBuilder->getSql();
```

**Resultado:**

```sql
INSERT INTO fake (str_col, numeric_col, date_col, null_col) VALUES ('str_val', 6.66, '2020-06-01', NULL)
```

#### 4.2.2 Componiendo sentencias `INSERT` mediate `array` indexados

Los métodos `setColumns()` y `addValues()` también soportan como parámetro un `array` indexado. Este mecanismo permite que el desarrallor(a) pueda preparar en `array` las columnas y valores a establecer y asignar dichos `array` con posterioridad al compositor.

**Ejemplo 2:** Sentencia `INSERT` con `array` indexados

```php
use xibalba\tuza\builder\Insert as InsertBuilder;

$cols = ['str_col', 'num_col', 'bool_col'];
$seeds = [
    ['str_val1', 1.11, false],
    ['str_val2', 2.22, ':bool_2'],
    ['str_val3', 3.33, true]
];

$iBuilder = new InsertBuilder("fake");

$iBuilder->setColumns(...$cols);
foreach ($seeds as $row) $iBuilder->addValues(...$row);

echo $iBuilder->getSql();
```

**Resultado:**

```sql
INSERT INTO fake (str_col, num_col, bool_col) VALUES ('str_val1', 1.11, 0), ('str_val2', 2.22, :bool_2), ('str_val3', 3.33, 1)
```

#### 4.2.2 Componiendo sentencias `INSERT` mediate el método `putValues()`

En el ejemplo anterior se mostró como establecer primero el listado de columnas a afectar y luego el conjunto de datos a insertar. Hay casos en los que se desea insertar una sola fila de registros. Mediante el método `putValues()` es posible establecer ambos listados (el de columnas y el de datos) en una sola llamada.

**Ejemplo 3:** Sentencia `INSERT` empleando diferentes formas el método `putValues()`

```php
use xibalba\tuza\builder\Insert as InsertBuilder;
use Ds\Map;

$seed = [
    "str_col" => ":str_val",
    "numeric_col" => 6.66,
    "date_col" => ":date_val",
    "null_col" => null
];

$iMapBuilder = new InsertBuilder("generic");
$iArrBuilder = new InsertBuilder("generic");

$iArrBuilder->putValues($seed);
$iMapBuilder->putValues(new Map($seed));

echo $iArrBuilder->getSql();
echo $iMapBuilder->getSql();
```

**Resultado:**

```sql
INSERT INTO generic (str_col, numeric_col, date_col, null_col) VALUES (:str_val, 6.66, :date_val, NULL)
```

#### 4.2.3 Componiendo sentencias `INSERT` mediate el método `pushValues()`

La clase `Insert` provee el método `pushValues()` que permite asignar los valores a partir de un `array` indexado de arregros de clave valor o, bien, de una instancia de `Ds\Vector` que contiene bien un conjunto de `array` de clave valor, o un conjunto de `Ds\Map`. Al emplear el método `pushValues()` se debe tener el cuidado de que el listado de columnas se tomará exclusivamente del primer conjunto. Este mecanismo está pensado para facilitar la composición de datos antes de establecerlos en el compositor de la Sentencia.

**Ejemplo 4:** Sentencia `INSERT` empleando diferentes formas el método `pushValues()`

```php
use xibalba\tuza\builder\Insert as InsertBuilder;
use Ds\Vector;
use Ds\Map;

$xSeeds = [
    [
        "str_col" => "str_val1",
        "numeric_col" => 1.11,
        "bool_col" => false
    ],
    [
        "str_col" => "str_val2",
        "numeric_col" => 2.22,
        "bool_col" => true
    ],
    [
        "str_col" => "str_val3",
        "numeric_col" => 3.33,
        "bool_col" => true
    ]
];

$vectorArr = new Vector();
$vectorMap = new Vector();

foreach($xSeeds as $s) $vectorArr->push($s);
foreach($xSeeds as $s) $vectorMap->push(new Map($s));

$iArrBuilder = new InsertBuilder("fake");
$iVectorMapBuilder = new InsertBuilder("fake");
$iVectorArrBuilder = new InsertBuilder("fake");

echo $iArrBuilder->getSql();
echo $iVectorMapBuilder->getSql();
echo $iVectorArrBuilder->getSql();
```

**Resultado:**

```sql
INSERT INTO fake (str_col, numeric_col, bool_col) VALUES ('str_val1', 1.11, 0), ('str_val2', 2.22, 1), ('str_val3', 3.33, 1)
```

El SQL resultante de las tres formas ejemplificadas (`$iArrBuilder`, `$iVectorMapBuilder` y `$iVectorArrBuilder`) es exactamente el mismo.

### 4.3 Componiendo sentencias `UPDATE`

Las sentencias `UPDATE` tienen por objetivo actualizar los datos en una tabla, usualmente en funcion de unas condiciones. Dicha sentencia está abstraida por la clase `Update`. Esta clase provee tres métodos distintos para establecer las columnas y sus datos correspondientes, así como la API completa de métodos de comparación que ya se observó en la clase `Select` para establecer las condiciones de actualización de la sentencia.

#### 4.3.1 Componiendo sentencias `UPDATE` mediante el método `set()`

El método `set()` espera dos parámetros: el primero es el nombre de la columna a afectar y el segundo el valor con el que se quiere actualizar dicho campo.

**Ejemplo 1:** Sentencia `UPDATE` empleando el método `set()`

```php
use xibalba\tuza\builder\Update as UpdateBuilder;

$uBuilder = new UpdateBuilder("fake");
$uBuilder->set("str_col", "text")
    ->set("numeric_col", 6)
    ->set("date_col", ":date_val")
    ->set("null_col", null)
    ->where()
        ->equals("bar", ":woo")
        ->subWhere('OR')
        ->greaterThan("baz", 6)
        ->notLike("zaz", "LAIK");

echo $uBuilder->getSql();
```

**Resultado:**

```sql
UPDATE fake SET str_col = 'text', numeric_col = 6, date_col = :date_val, null_col = NULL WHERE (fake.bar = :woo) AND ( (fake.baz > 6) OR (fake.zaz NOT LIKE 'LAIK') )
```

Como puede observarse, puede llamarse al método `set()` para establecer un par columna-dato tantas veces como sea necesario. Luego, la llamada al método `where()` prepara al compositor para establecer las condiciones siguiendo la misma lógica que con `Select`.

#### 4.3.2 Componiendo sentencias `UPDATE` mediante el método `setValues()`

Para los casos en que se tiene un `array` de clave-valor donde cada clave corresponde a una columna y el valor al dato o *placeholder* que se le desea asignar, es preferible emplear el método `setValues()`. Este método también soporta un parámetro de tipo `Ds\Map`.

**Ejemplo 2:** Sentencia `UPDATE` empleando el método `setValues()`

```php
use xibalba\tuza\builder\Update as UpdateBuilder;

$seed = [
    "str_col" => "text",
    "numeric_col" => 6,
    "date_col" => ":date_val",
    "null_col" => null
];

$uBuilder = new UpdateBuilder("fake");
$uBuilder->setValues($seed)
    ->equals("bar", ":woo")
    ->subWhere('OR')
        ->greaterThan("baz", 6)
            ->notLike("zaz", "LAIK");

echo $uBuilder->getSql();
```

**Resultado:**

```sql
UPDATE fake SET str_col = 'text', numeric_col = 6, date_col = :date_val, null_col = NULL WHERE (fake.bar = :woo) AND ( (fake.baz > 6) OR (fake.zaz NOT LIKE 'LAIK') )
```

Como puede observarse, el resultado es exactamente igual que en el *Ejemplo 1*.

#### 4.3.3 Componiendo sentencias `UPDATE` mediante el método `setMap()`

Como alternativa al método `setValues()`, la clase `Update` provee el método `setMap()` que, como lo indica su nombre, únicamente soporta como parámetro un argumento de tipo `Ds\Map`.

**Ejemplo 3:** Sentencia `UPDATE` empleando el método `setMap()`

```php
use xibalba\tuza\builder\Update as UpdateBuilder;
use Ds\Map;

$seed = new Map();
$seed->put("str_col", "text");
$seed->put("numeric_col", 6);
$seed->put("date_col", ":date_val");
$seed->put("null_col", null);

$uBuilder = new UpdateBuilder("fake");
$uBuilder->setMap($seed)
    ->equals("bar", ":woo")
    ->subWhere('OR')
        ->greaterThan("baz", 6)
            ->notLike("zaz", "LAIK");

echo $uBuilder->getSql();
```

**Resultado:**

```sql
UPDATE fake SET str_col = 'text', numeric_col = 6, date_col = :date_val, null_col = NULL WHERE (fake.bar = :woo) AND ( (fake.baz > 6) OR (fake.zaz NOT LIKE 'LAIK') )
```

Al igual que en *Ejemplo 1* y *Ejemplo 2*, el resultado es exactamente el mismo.

### 4.3 Componiendo sentencias `DELETE`

Las sentencias `DELETE` eliminan datos de una tabla objetivo en función de unas condiciones determinadas. Dicha sentencias es abstraida en Tuza por la clase `Delete`. Al igual que en las otras clases compositoras, el nombre de la tabla es establecido en el constructor de la clase, y las condiciones son establecidas mediante la llamada los métodos de comporación tras llamar al método `where()`.

**Ejemplo 1:** Sentencia `DELETE`

```php
use xibalba\tuza\builder\Delete as DeleteBuilder;

$dBuilder = new DeleteBuilder("fake");
$dBuilder->where()
    ->equals("bar", ":woo")
    ->subWhere('OR')
        ->greaterThan("baz", 6)
        ->notLike("zaz", "LAIK");

echo $dBuilder->getSql();
```

**Resultado:**

```sql
DELETE FROM fake  WHERE (fake.bar = :woo) AND ( (fake.baz > 6) OR (fake.zaz NOT LIKE 'LAIK') )
```