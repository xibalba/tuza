<?php
/**
 * @copyright	2014 - 2019 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\composer;

use xibalba\tuza\statement\Select as SelectStatement;

/**
 * This class provide an OOP API for query building.
 *
 * @package xibalba\tuza\syntax
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Generic {

	private $selectStatement;

	public function select() : Generic {

	}

	public function from() : Generic {

	}

	public function getSql() : string {

	}
}
