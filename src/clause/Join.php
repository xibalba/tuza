<?php
/**
 * @copyright	2014 - 2020 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\clause;

use xibalba\tuza\clause\traits\WhereAware;

use xibalba\tuza\expression\traits\ColumnsAware;
use xibalba\tuza\expression\Factory as FactoryExpression;

use xibalba\tuza\clause\Where as WhereClause;
use xibalba\tuza\syntax\Target as TargetSyntax;

use Ds\Vector;
use Ds\Map;

class Join {
	use ColumnsAware, WhereAware;

	const JOIN_LEFT = "LEFT";
	const JOIN_RIGHT = "RIGHT";
	const JOIN_INNER = "INNER";
	const JOIN_CROSS = "CROSS";

	protected $_type;

	public function __construct(TargetSyntax $target, string $type) {
		$this->setTarget($target);
		$this->setType($type);
	}

	public function on() : Where {
		if($this->hasWhere()) return $this->getWhere();
		$this->setWhere(new WhereClause($this->getTarget()));
		return $this->getWhere();
	}

	public function setColumns($columns) {
		if (is_array($columns)) {
			if (array_keys($columns) === range(0, count($columns) - 1)) $columns = new Vector($columns);
			else $columns = new Map($columns);
		}

		if ($columns instanceof Vector) $this->setColumnsExpression( FactoryExpression::createColumns($columns, $this->getTarget()) );
		else if ($columns instanceof Map) $this->setColumnsExpression( FactoryExpression::createColumnsWithAlias($columns, $this->getTarget()) );
		else throw new \InvalidArgumentException("In order to set the columns expression you must provide an array, a Vector or a Map object of columns.");

		return $this;
	}

	public function setType(string $type) {
		switch($type) {
			case self::JOIN_LEFT:
			case self::JOIN_RIGHT:
			case self::JOIN_INNER:
			case self::JOIN_CROSS:
				$this->_type = $type;
				break;
			default:
				throw new \InvalidArgumentException("Invalid join type specified, must be one fo LEFT, RIGHT, INNER or CROO, however '" . $type . "' was found");
		}
	}

	public function getType() : string {
		return $this->_type;
	}
}