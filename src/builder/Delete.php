<?php
/**
 * @copyright	2014 - 2024 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\builder;

use xibalba\tuza\builder\interfaces\Whereable;
use xibalba\tuza\builder\traits\WhereAdapter;

use xibalba\tuza\statement\Delete as DeleteStatement;

use xibalba\tuza\syntax\Factory as FactorySyntax;

use xibalba\tuza\composer\Delete as DeleteComposer;

/**
 * This class provide an APP API for DELETE query building
 */
class Delete implements Whereable {
	use WhereAdapter;

	private DeleteStatement $__deleteStatement;

	protected function _getStatement() : DeleteStatement {
		return $this->__deleteStatement;
	}

	public function __construct(?string $targetName = null, string $schema = "") {
		$this->__deleteStatement = new DeleteStatement();
		if(!is_null($targetName)) $this->setTarget($targetName, $schema);
	}

	public function setTarget(string $name, string $schema = "") : Delete {
		if($this->_getStatement()->hasTarget()) throw new \RuntimeException("You can set the builder's Target once.");
		$this->_getStatement()->setTarget(FactorySyntax::createTarget(name: $name, schema: $schema));

		return $this;
	}

	public function getSql() : string {
		return DeleteComposer::compose($this->_getStatement());
	}
}