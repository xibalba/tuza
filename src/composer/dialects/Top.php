<?php

namespace xibalba\tuza\composer\dialects;

use xibalba\tuza\clause\dialects\Top as TopClause;

class Top {
	public static function compose(TopClause $clause) : string {
		return "TOP " . $clause->getTop();
	}
}