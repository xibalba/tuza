<?php
/**
 * @copyright	2014 - 2020 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\composer;

use xibalba\tuza\statement\Insert as InsertStatement;

use \Ds\Vector;

class Insert {
	public static function compose(InsertStatement $statement) : string {
		$table = $statement->getTarget()->getQualifiedName();
		$columns = Columns::composeList($statement->getColumnsExpression()->getColumns());
		$values = static::composeValues($statement->getValues());

		return "INSERT INTO $table ($columns) VALUES $values";
	}

	public static function composeValues(Vector $valuesRows) : string {
		$rows = new Vector();

		foreach($valuesRows as $row) {
			$rows->push("(" . Placeholder::composeValidSqlValues($row) . ")");
		}

		return $rows->join(", ");
	}
}