<?php

/**
 * @copyright    2014 - 2020 Xibalba Lab.
 * @license      http://opensource.org/licenses/MIT
 * @link         https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\composer;

use xibalba\tuza\clause\Where as WhereClause;

use xibalba\tuza\syntax\Column as ColumnSyntax;

use xibalba\tuza\composer\Columns as ColumnComposer;
use xibalba\tuza\composer\Placeholder as PlaceholderComposer;

use \Ds\Vector;

class Where {
	public static function compose(WhereClause $where) : string {
		$clauses = static::composeClauses($where);

		if($clauses->count() > 0) return trim($clauses->join(" "));
		return "";
	}

	public static function composeClause(WhereClause $where) : string {
		$strWhere = static::compose($where);
		if(mb_strlen($strWhere) > 0) return "WHERE " . $strWhere;

		return "";
	}

	public static function composeClauses(WhereClause $where) : Vector {
		$clauses = new Vector();

		$clauses->push(static::composeComparisons($where));
		$clauses->push(static::composeSubWheres($where));

		return $clauses;
	}

	public static function composeConjunction(string $conjunction) : string {
		return ' ' . $conjunction . ' ';
	}

	public static function composeComparisons(WhereClause $where) : string {
		$parts = new Vector();
		$comparisons = $where->getComparisons();

		// -- Compose binary comparisons -- //

		foreach ($comparisons as $comp) {
			$clause  = static::composePartialCondition($comp->get("subject"));
			$clause .= static::composeConjunction($comp->get("conjunction"));
			$clause .= static::composePartialCondition($comp->get("target"));

			$parts->push("(" . $clause . ")");
		}

		// -- Compose IN and NOT IN comparisons -- //

		$ins = $where->getIns();
		$notIns = $where->getNotIns();

		foreach ($ins as $column => $values) $parts->push("(" . static::_composeIn($column, $values, "IN") . ")");
		foreach ($notIns as $column => $values) $parts->push("(". static::_composeIn($column, $values, "NOT IN") . ")");

		// -- Compose BETWEEN comparisons -- //

		$betweens = $where->getBetweens();

		foreach ($betweens as $b) {
			$clause  = static::composePartialCondition($b->get("subject"));
			$clause .= static::composeConjunction($b->get("operator"));
			$clause .= static::composePartialCondition($b->get("min"));
			$clause .= static::composeConjunction("AND");
			$clause .= static::composePartialCondition($b->get("max"));

			$parts->push("(" . $clause . ")");
		}

		return $parts->join(static::composeConjunction($where->getConjunction()));
	}

	protected static function _composeIn(ColumnSyntax $column, Vector $values, string $operator) : string {
		$part = ColumnComposer::composeSingle($column);
		$part .= " $operator ";

		$procesedVals = new Vector();
		foreach ($values as $val) $procesedVals->push(PlaceholderComposer::composeValidSqlValue($val));

		$part .= "(" . $procesedVals->join(", ") . ")";
		return $part;
	}

	public static function composePartialCondition($subject) : string {
		if ($subject instanceof ColumnSyntax) return ColumnComposer::composeSingle($subject);
		else return PlaceholderComposer::composeValidSqlValue($subject);
	}

	public static function composeSubWheres(WhereClause $where) : string {
		$subWheres = $where->getSubWheres();
		$subWhereClauses = new Vector();

		if($subWheres->count() === 0) return "";
		foreach ($subWheres as $sw) $subWhereClauses->push('( ' . trim(static::compose($sw)) . ' )');

		return $where->getConjunction() . ' ' . $subWhereClauses->join("");
	}
}