<?php
/**
 * @copyright	2014 - 2020 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\expression;

use xibalba\tuza\expression\Columns as ColumnsExpression;

use xibalba\tuza\syntax\Factory as SyntaxFactory;
use xibalba\tuza\syntax\Target as TargetSyntax;

use \Ds\Vector;
use \Ds\Map;

class Factory {
	public static function createColumns(Vector $columns, TargetSyntax $target) : ColumnsExpression {
		return  new ColumnsExpression(SyntaxFactory::createColumns($columns, $target));
	}

	public static function createColumnsWithAlias(Map $columns, TargetSyntax $target) : ColumnsExpression {
		return  new ColumnsExpression(SyntaxFactory::createColumnsWithAlias($columns, $target));
	}
}
