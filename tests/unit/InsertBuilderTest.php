<?php

namespace guatuza;

use PHPUnit\Framework\TestCase;

use xibalba\tuza\builder\Insert as InsertBuilder;

use \Ds\Vector;
use \Ds\Map;

class InsertBuilderTest extends TestCase {
	public function testSetTargetException() {
		$iBuilder = new InsertBuilder("fake");
		$this->expectException(\RuntimeException::class);
		$iBuilder->setTarget("noValid");
	}

	public function testPutValuesException() {
		$iBuilder = new InsertBuilder("fake");
		
		$malformedArray = ["a", "b", "c"];
		$this->expectException(\InvalidArgumentException::class);
		$iBuilder->putValues($malformedArray);
		
		$emptyMap = new Map();
		$this->expectException(\InvalidArgumentException::class);
		$iBuilder->putValues($emptyMap);

		$invalidVect = new Vector();
		$invalidVect->push(["a", "b", "c"]);
		$this->expectException(\InvalidArgumentException::class);
		$iBuilder->pushValues($invalidVect);
	}

	public function testPushValuesException() {
		$iBuilder = new InsertBuilder("fake");
		$malformedArray = ["a", "b" => "d", "c"];
		$this->expectException(\InvalidArgumentException::class);
		$iBuilder->pushValues($malformedArray);
	}

	public function testSimpleAdd() {
		$expected = "INSERT INTO fake (str_col, numeric_col, date_col, null_col) VALUES ('str_val', 6.66, '2020-06-01', NULL)";

		$iBuilder = new InsertBuilder("fake");

		// Basic behaviour is setting first the columns list and then the values.
		// Order is important. If values call have a different number of ellement, an exception is rised
		$iBuilder->setColumns("str_col", "numeric_col", "date_col", "null_col");
		$iBuilder->addValues("str_val", 6.66, '2020-06-01', null);

		$this->assertSame($expected, $iBuilder->getSql());
	}

	public function testMultipleAdd() {
		$expected = "INSERT INTO fake (str_col, num_col, bool_col) VALUES ('str_val1', 1.11, 0), ('str_val2', 2.22, :bool_2), ('str_val3', 3.33, 1)";

		$cols = ['str_col', 'num_col', 'bool_col'];
		$seeds = [
			['str_val1', 1.11, false],
			['str_val2', 2.22, ':bool_2'],
			['str_val3', 3.33, true]
		];

		$iBuilder = new InsertBuilder("fake");

		$iBuilder->setColumns(...$cols);
		foreach ($seeds as $row) $iBuilder->addValues(...$row);

		$this->assertSame($expected, $iBuilder->getSql());
	}

	public function testPushValues() {
		$expected = "INSERT INTO fake (str_col, numeric_col, bool_col) VALUES ('str_val1', 1.11, 0), ('str_val2', 2.22, 1), ('str_val3', 3.33, 1)";

		$xSeeds = [
			[
				"str_col" => "str_val1",
				"numeric_col" => 1.11,
				"bool_col" => false
			],
			[
				"str_col" => "str_val2",
				"numeric_col" => 2.22,
				"bool_col" => true
			],
			[
				"str_col" => "str_val3",
				"numeric_col" => 3.33,
				"bool_col" => true
			]
		];

		$vectorArr = new Vector();
		$vectorMap = new Vector();

		foreach($xSeeds as $s) $vectorArr->push($s);
		foreach($xSeeds as $s) $vectorMap->push(new Map($s));

		$iArrBuilder = new InsertBuilder("fake");
		$iVectorMapBuilder = new InsertBuilder("fake");
		$iVectorArrBuilder = new InsertBuilder("fake");

		$iArrBuilder->pushValues($xSeeds);
		$iVectorArrBuilder->pushValues($vectorArr);
		$iVectorMapBuilder->pushValues($vectorMap);

		$this->assertSame($expected, $iArrBuilder->getSql());
		$this->assertSame($expected, $iVectorArrBuilder->getSql());
		$this->assertSame($expected, $iVectorMapBuilder->getSql());

	}

	public function testPersonalPlaceholder() {
		$expected = "INSERT INTO generic (str_col, numeric_col, date_col, null_col) VALUES (:str_val, 6.66, :date_val, NULL)";

		$seed = [
			"str_col" => ":str_val",
			"numeric_col" => 6.66,
			"date_col" => ":date_val",
			"null_col" => null
		];

		$iMapBuilder = new InsertBuilder("generic");
		$iArrBuilder = new InsertBuilder("generic");

		// values expect a map for a single call to set columns and values.
		$iArrBuilder->putValues($seed);
		$iMapBuilder->putValues(new Map($seed));

		$this->assertSame($expected, $iMapBuilder->getSql());
		$this->assertSame($expected, $iArrBuilder->getSql());
	}
}