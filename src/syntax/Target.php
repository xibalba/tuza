<?php
/**
 * @copyright	2014 - 2023 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\syntax;

use Ds\Vector;

//use xibalba\tuza\manipulation\ColumnQuery;

/**
 * Class Target
 * This class is an abstraction that wraps a target FROM syntax part, that means a TABLE or VIEW name.
 * Notice that this abstraction is just a wrapper for the SQL target FROM syntax,
 * not the SQL TABLE o VIEW Structure, so this class is not compsed by a column wrapper or similar,
 * instead this class only abstract the 4 attributes of the SQL target FROM syntax:
 *   * The table or view name
 *   * An optial alias
 *   * An optional schema name
 *   * A flag to indicate if is a view or not.
 *
 * @package xibalba\tuza\syntax
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Target {

	/**
	 * @var string
	 */
	private string $__name;

	/**
	 * @var string
	 */
	private string $__alias;

	/**
	 * @var string
	 */
	private string $__schema;

	/**
	 * @var bool
	 */
	private bool $__isView = false;

	/**
	 * @param string $name
	 * @param string $alias
	 * @param string $schema
	 */
	public function __construct(string $name, string $alias = "", string $schema = "") {
		$this->__name = $name;
		$this->setAlias($alias);
		$this->setSchema($schema);
	}

	/**
	 * @return string
	 */
	public function __toString() {
		return $this->getQualifiedName();
	}

	/**
	 * @param bool $value
	 */
	public function setIsView(bool $value) {
		$this->__isView = $value;
	}

	/**
	 * @return boolean
	 */
	public function isView() : bool {
		return $this->__isView;
	}

	/**
	 * @return string
	 */
	public function getName() : string {
		return $this->__name;
	}

	/**
	 * @return string
	 */
	public function getAlias() : string {
		return $this->__alias;
	}

	/**
	 * @return string
	 */
	public function getSchema() : string {
		return $this->__schema;
	}

	/**
	 * @return boolean
	 */
	public function hasAlias() : bool {
		return \mb_strlen($this->__alias) > 0  ? true : false;
	}

	/**
	 * @return bool
	 */
	public function hasSchema() : bool {
		return \mb_strlen($this->__schema) ? true : false;
	}

	/**
	 * Retrive the quealified name for the table syntax as `schema.table`
	 * If is not set a schema then will return the table name alone.
	 *
	 * @return string
	 */
	public function getQualifiedName() : string {
		return $this->hasSchema() ? $this->getSchema() . "." . $this->getName() : $this->getName();
	}

	/**
	 * @param string $alias
	 */
	public function setAlias(string $alias) : void {
		$this->__alias = $alias;
	}

	/**
	 * @param string $schema
	 */
	public function setSchema(string $schema) : void {
		$this->__schema = $schema;
	}
}
