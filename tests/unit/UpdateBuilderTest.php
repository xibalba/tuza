<?php

namespace guatuza;

use PHPUnit\Framework\TestCase;

use xibalba\tuza\builder\Update as UpdateBuilder;

use \Ds\Map;

class UpdateBuilderTest extends TestCase {
	public function testSimpe() {
		$expected = "UPDATE fake SET str_col = 'text', numeric_col = 6, date_col = :date_val, null_col = NULL"
			. " WHERE (fake.bar = :woo) AND ( (fake.baz > 6) OR (fake.zaz NOT LIKE 'LAIK') )";

		$mapBuilder = new UpdateBuilder("fake");
		$mapQuery = $mapBuilder
			->setMap(new Map([
				"str_col" => "text",
				"numeric_col" => 6,
				"date_col" => ":date_val",
				"null_col" => null
			]))
			->where()
				->equals("bar", ":woo")
				->subWhere('OR')
					->greaterThan("baz", 6)
					->notLike("zaz", "LAIK")
			->getSql();

		$uBuilder = new UpdateBuilder("fake");
		$uQuery = $uBuilder->set("str_col", "text")
			->set("numeric_col", 6)
			->set("date_col", ":date_val")
			->set("null_col", null)
			->where()
				->equals("bar", ":woo")
				->subWhere('OR')
				->greaterThan("baz", 6)
				->notLike("zaz", "LAIK")
			->getSql();

		$arrBuilder = new UpdateBuilder("fake");
		$aQuery = $arrBuilder->setValues([
			"str_col" => "text",
			"numeric_col" => 6,
			"date_col" => ":date_val",
			"null_col" => null
		])
		->where()
				->equals("bar", ":woo")
				->subWhere('OR')
				->greaterThan("baz", 6)
				->notLike("zaz", "LAIK")
			->getSql();

		$this->assertSame($expected, $mapQuery);
		$this->assertSame($expected, $uQuery);
		$this->assertSame($expected, $aQuery);
	}
}