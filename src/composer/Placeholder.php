<?php
/**
 * @copyright	2014 - 2024 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\composer;

use \Ds\Vector;

class Placeholder {

	public static function composeValidSqlValue($value) : string {
		switch (gettype($value)) {
			case 'NULL':
				return static::composeNull();
			case 'string':
				return static::composeString($value);
			case 'boolean':
				return static::composeBoolean($value);
			case 'integer':
			case 'double':
				return $value;
			default:
				throw new \InvalidArgumentException("Unssuported value type.");
		}
	}

	public static function composeValidSqlValues(Vector $values) : string {
		$vals = new Vector();
		foreach ($values as $val) $vals->push(static::composeValidSqlValue($val));
		return $vals->join(", ");
	}

	public static function composeNull() : string {
		return 'NULL';
	}

	public static function composeString(string $value) : string {
		$value = trim($value);

		if($value[0] === ':') return $value;
		return "'" . $value . "'";
	}

	public static function composeBoolean(bool $value) : string {
		return \filter_var($value, FILTER_VALIDATE_BOOLEAN) ? '1' : '0';
	}
}