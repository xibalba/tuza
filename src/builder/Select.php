<?php
/**
 * @copyright	2014 - 2023 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\builder;

use xibalba\tuza\builder\interfaces\Whereable;
use xibalba\tuza\builder\traits\WhereAdapter;
use xibalba\tuza\statement\Select as SelectStatement;

use xibalba\tuza\expression\Factory as FactoryExpression;
use xibalba\tuza\syntax\Factory as FactorySyntax;

use xibalba\tuza\syntax\Column as ColumnSyntax;

use xibalba\tuza\composer\Select as SelectComposer;

use \Ds\Vector;
use \Ds\Map;

/**
 * This class provide an OOP API for SELECT query building.
 *
 * @package xibalba\tuza\builder
 * @author Yeshua Rodas <yrodas@upnfm.edu.hn> ☭
 */
class Select implements Whereable {
	use WhereAdapter;

	protected SelectStatement $_selectStatement;

	/**
	 * Select constructor.
	 * Begin the SELECT STATEMENT composing.
	 * You can set the `FROM` clause target. Otherwise, you must explicitly call the `from()` method with
	 * same signature.
	 *
	 * @param string|null $target
	 * @param string $alias
	 * @param string $schema
	 */
	public function __construct(?string $target = null, string $alias = "", string $schema = "") {
		$this->_selectStatement = new SelectStatement();

		if(!is_null($target)) $this->from($target, $alias, $schema);
		else if (!empty($alias) || !empty($schema))
			throw new \InvalidArgumentException("You must provide a target name (table or view).");
	}

	private function __setColumnsWithAlias(Map $columns) {
		$ss = $this->_getStatement();
		$ss->setColumnsExpression( FactoryExpression::createColumnsWithAlias($columns, $ss->getTarget()) );
	}

	private function __setColumns(string ...$columns) {
		$ss = $this->_getStatement();

		$vc = new Vector();
		if(count($columns) > 0) $vc->push(...$columns);

		$ss->setColumnsExpression( FactoryExpression::createColumns($vc, $ss->getTarget()) );
	}

	protected function _getStatement() : SelectStatement {
		if (is_null($this->_selectStatement)) throw new \RuntimeException("A SELECT statement has not been builded yet.");
		return $this->_selectStatement;
	}

	/**
	 * Set SELECT as SELECT DISTINCT
	 * @return $this
	 */
	public function distinct() : Select {
		$this->_getStatement()->distinct();
		return $this;
	}

	/**
	 * Adds the FROM expression.
	 *
	 * @param string $target
	 * @param string $alias
	 * @param string $schema
	 * @return $this
	 */
	public function from(string $target, string $alias = "", string $schema = "") : Select {
		if($this->_getStatement()->hasTarget()) throw new \RuntimeException("the `from()` method must be called once.");
		$this->_getStatement()->setTarget(FactorySyntax::createTarget($target, $alias, $schema));
		return $this;
	}

	/**
	 * Set columns to select
	 * @param string ...$columns
	 * @return $this
	 */
	public function setColumns(string ...$columns) : Select {
		if(count($columns) === 0) throw new \InvalidArgumentException("You must provide a leat one column string to set as column expression.");
		$this->__setColumns(...$columns);
		return $this;
	}

	/**
	 * Put columns with alias from a key-value pair structure as an array or a Map.
	 * @param $columns
	 * @return $this
	 */
	public function putColumns(array|Map $columns) : Select {
		if(is_array($columns)) {
			if(count($columns) > 0 && array_keys($columns) !== range(0, count($columns) - 1)) $this->__setColumnsWithAlias(new Map($columns));
			else throw new \InvalidArgumentException("In order to put a columns expression to the statement you must provide a key-value array or a Map object of columns.");
		}
		else if ($columns->count() > 0) $this->__setColumnsWithAlias($columns);
		else throw new \InvalidArgumentException("You must provide a key-value pairs array or a Map instace with at least one pair of columns.");

		return $this;
	}

	/**
	 * Retrive a column description as a ColumnSyntax instance.
	 * @param string $name
	 * @return ColumnSyntax
	 */
	public function getColumn(string $name) : ColumnSyntax {
		return $this->_getStatement()->getColumn($name);
	}

	/**
	 * Adds a function, like MAX, SUM, MIN, etc., as part of a columns expression.
	 * @param string $functionName
	 * @param string $column
	 * @param string|null $alias
	 * @return $this
	 */
	public function addFunctionOnColumn(string $functionName, string $column, string $alias = "") : Select {
		$ss = $this->_getStatement();

		if(!$ss->hasColumnsExpression()) $this->__setColumns();

		$ss->getColumnsExpression()->addFunction(
			$functionName,
			[new ColumnSyntax($column, $ss->getTarget())],
			$alias
		);

		return $this;
	}

	/**
	 * Add a COUNT function to the actual columns expression.
	 * @param string $column
	 * @param string $alias
	 * @return $this
	 */
	public function count(string $column = ColumnSyntax::ALL, string $alias = "") : Select {
		return $this->addFunctionOnColumn("COUNT", $column, $alias);
	}

	// --- JOIN adapters --- //

	/**
	 * Prepare Whereable calls (equal, like, less, etc) to a JOIN clause.
	 * You must call a `{x}Join()` method before call `on()` method.
	 * @return $this
	 */
	public function on() : Select {
		$ss = $this->_getStatement();
		if(!$ss->hasJoin()) throw new \RuntimeException("You must call a `{x}Join()` method before call `on()` method.");

		$this->_setLastWhere($ss->getAllJoins()->last()->on());

		return $this;
	}

	/**
	 * Adds an INNER JOIN clause to the builder.
	 * @param string $target
	 * @param null $selecteableColumns
	 * @return $this
	 */
	public function innerJoin(string $target, $selecteableColumns = null) : Select {
		$this->_getStatement()->innerJoin($target, $selecteableColumns);
		return $this;
	}

	/**
	 * Adds an LEFT JOIN clause to the builder.
	 * @param string $target
	 * @param null $selecteableColumns
	 * @return $this
	 */
	public function leftJoin(string $target, $selecteableColumns = null) : Select {
		$join = $this->_getStatement()->leftJoin($target, $selecteableColumns);
		return $this;
	}

	/**
	 * Adds an RIGHT JOIN clause to the builder.
	 * @param string $target
	 * @param null $selecteableColumns
	 * @return $this
	 */
	public function rightJoin(string $target, $selecteableColumns = null) : Select {
		$this->_getStatement()->rightJoin($target, $selecteableColumns);
		return $this;
	}

	/**
	 * Adds an CROSS JOIN clause to the builder.
	 * @param string $target
	 * @param null $selecteableColumns
	 * @return $this
	 */
	public function crossJoin(string $target, $selecteableColumns = null) : Select {
		$this->_getStatement()->crossJoin($target, $selecteableColumns);
		return $this;
	}

	// --- LIMIT adapter --- //

	/**
	 * Adds a LIMIT clause to the builder.
	 * Take care of call this method only if your target DBMS suuport it (like SQLite or MariaDB).
	 * @param int $limit
	 * @param int|null $offset
	 * @return $this
	 */
	public function limit(int $limit, ?int $offset = null) : Select {
		$this->_getStatement()->setLimit($limit, $offset);
		return $this;
	}

	// --- TOP adapter --- //

	/**
	 * Adds a TOP clause to the builder.
	 * Take care of call this method only if your target DBMS suuport it (like MS SQL Server).
	 * @param int $top
	 * @return $this
	 */
	public function top(int $top) : Select {
		$this->_getStatement()->setTop($top);
		return $this;
	}

	/**
	 * Retrive the composed query string.
	 * @return string
	 */
	public function getSql() : string {
		return SelectComposer::compose($this->_getStatement());
	}
}
