<?php
/**
 * @copyright	2014 - 2024 Xibalba Lab.
 * @license 	http://opensource.org/licenses/MIT
 * @link		https://gitlab.com/xibalba/tuza
 */

namespace xibalba\tuza\builder;

use xibalba\tuza\statement\Insert as InsertStatement;

use xibalba\tuza\syntax\Factory as FactorySyntax;
use xibalba\tuza\expression\Factory as FactoryExpression;

use xibalba\tuza\composer\Insert as InsertComposer;

use \Ds\Vector;
use \Ds\Map;

/**
 * This class provide a INSERT statement builder.
 */
class Insert {
	private InsertStatement $__insertStatement;

	private function __getStatement() : InsertStatement {
		return $this->__insertStatement;
	}

	/**
	 * Insert constructor.
	 * @param string|null $targetName Optional target name
	 * @param string $schema Optional schema name
	 */
	public function __construct(?string $targetName = null, string $schema = "") {
		$this->__insertStatement = new InsertStatement();
		if(!is_null($targetName)) $this->setTarget($targetName, $schema);
	}

	/**
	 * Set builder's target, a table or views name. Optional a schema name can be setted.
	 *
	 * @param string $name Target name
	 * @param string $schema Optional schema name
	 * @return $this
	 */
	public function setTarget(string $name, string $schema = "") : Insert {
		if($this->__getStatement()->hasTarget()) throw new \RuntimeException("You can set the builder's Target once.");
		$this->__getStatement()->setTarget(FactorySyntax::createTarget(name: $name, schema: $schema));

		return $this;
	}

	/**
	 * Set those columns for insert to. As the sentence itself, you must take care about the order on those columns setted.
	 *
	 * @param string ...$columns Columns list for insert to.
	 * @return $this
	 */
	public function setColumns(string ...$columns) : Insert {
		if(count($columns) === 0) throw new \InvalidArgumentException("You must set at least one column.");

		$ss = $this->__getStatement();

		$vc = new Vector();
		$vc->push(...$columns);

		$ss->setColumnsExpression( FactoryExpression::createColumns($vc, $ss->getTarget()) );

		return $this;
	}

	/**
	 * Put a columns expresion and its correspond values from a Map instance.
	 *
	 * @param Map $columnsAndValues
	 */
	private function __putValues(Map $columnsAndValues) {
		$this->setColumns(...$columnsAndValues->keys());
		$this->addValues(...$columnsAndValues->values());
	}

	/**
	 * Add values to the statement.
	 * The provided values must match setted columns quantity.
	 *
	 * @param mixed ...$values
	 * @return $this
	 */
	public function addValues(...$values) : Insert {
		if(!$this->__getStatement()->hasColumnsExpression()) throw new \RuntimeException("You must set a columns expression before add values.");
		if(count($values) === 0) throw new \InvalidArgumentException("You must set at least one value.");

		$this->__getStatement()->pushValues(new Vector($values));
		return $this;
	}

	/**
	 * Put columns and values from a key-pair structure (array or Map).
	 *
	 * @param array | Map $values
	 * @return $this
	 */
	public function putValues(array | Map $values) : Insert {
		if(is_array($values)) {
			if(count($values) > 0 && array_keys($values) !== range(0, count($values) - 1)) $this->__putValues(new Map($values));
			else throw new \InvalidArgumentException("You must provide a key-value pairs array or a Map instace with at least one pair.");
		}
		else if ($values->count() > 0) $this->__putValues($values);
		else throw new \InvalidArgumentException("You must provide a key-value pairs array or a Map instace with at least one pair.");

		return $this;
	}

	/**
	 * Push values on builder.
	 *
	 * @param $values
	 * @return $this
	 */
	public function pushValues(array | Vector $values) : Insert {
		if(is_array($values)) {
			if(count($values) > 0 && array_keys($values) === range(0, count($values) - 1)) $values = new Vector($values);
			else throw new \InvalidArgumentException("You must provide mapped arrays or Map instaces as content of the argument.");
		}

		// Si es un arreglo indexado, verificar que cada elemento sea un arreglo clave-valor o una instancia de Map
		$columns = [];
		$firstRow = $values->get(0);
		if($firstRow instanceof Map) $columns = $firstRow->keys();
		else if(is_array($firstRow) && count($firstRow) > 0 && array_keys($firstRow) !== range(0, count($firstRow) - 1)) {
			$columns = array_keys($firstRow);
		}
		else throw new \InvalidArgumentException("You must provide mapped arrays or Map instaces as content of the argument.");

		$this->setColumns(...$columns);

		foreach ($values as $el) {
			if(is_array($el) && count($el) > 0 && array_keys($el) !== range(0, count($el) - 1)) {
				if($columns === array_keys($el)) $this->addValues(...array_values($el));
				else throw new \InvalidArgumentException("A dismatch column definition was found.");
			}
			else if($el instanceof Map) {
				if($columns == $el->keys()) $this->addValues(...$el->values());
				else throw new \InvalidArgumentException("A dismatch column definition was found.");
			}
			else throw new \InvalidArgumentException("You must provide mapped arrays or Map instaces as content of the argument.");
		}

		return $this;
	}

	/**
	 * Retrive the composed INSERT statement string.
	 * @return string
	 */
	public function getSql() : string {
		return InsertComposer::compose($this->__getStatement());
	}
}