<?php

namespace xibalba\tuza\composer\dialects;

use xibalba\tuza\clause\dialects\Limit as LimitClause;

class Limit {
	public static function compose(LimitClause $clause) : string {
		return "LIMIT " . $clause->getLimit()
			. ($clause->hasOffset() ? " OFFSET " . $clause->getOffset() : "");
	}
}