<?php

namespace guatuza;

use PHPUnit\Framework\TestCase;

use xibalba\tuza\builder\Delete as DeleteBuilder;

class DeleteBuilderTest extends TestCase {
	public function testSimple() {
		$expexted = "DELETE FROM fake"
			. " WHERE (fake.bar = :woo) AND ( (fake.baz > 6) OR (fake.zaz NOT LIKE 'LAIK') )";

		$dBuilder = new DeleteBuilder("fake");
		$query = $dBuilder
			->where()
				->equals("bar", ":woo")
				->subWhere('OR')
					->greaterThan("baz", 6)
					->notLike("zaz", "LAIK")
			->getSql();

		$this->assertSame($expexted, $query);
	}
}