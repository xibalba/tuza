<?php

namespace guatuza;

use Ds\Vector;
use PHPUnit\Framework\TestCase;

use xibalba\tuza\syntax\Target as TargetSyntax;
use xibalba\tuza\syntax\Column as ColumnSyntax;
use xibalba\tuza\composer\Columns as ColumnsComposer;

class ColumnTest extends TestCase {
	public function testIsAll() {
		$target = new TargetSyntax("table");
		$column = new ColumnSyntax("column", $target);

		$this->assertFalse($column->isAll());

		$column->setName(ColumnSyntax::ALL);

		$this->assertTrue($column->isAll());
	}

	public function testName() {
		$expectedSimple = "schema.table.column";
		$expectedAlias = "tableAlias.column AS columnAlias";

		$target = new TargetSyntax(name: "table", schema: "schema");
		$aliasedTable = new TargetSyntax("table", "tableAlias", "schema");

		$column = new Vector();
		$column->push(new ColumnSyntax("column", $target));
		$aliasedColumn = new ColumnSyntax("column", $aliasedTable, "columnAlias");

		$this->assertSame($expectedSimple, ColumnsComposer::compose($column));
		$this->assertSame($expectedAlias, ColumnsComposer::composeWithAlias($aliasedColumn));
	}
}